﻿using UnityEngine;
using System.Collections;

public class DontDestroy : MonoBehaviour {

    private static DontDestroy _instance = null;

    void Awake()
    {
        if (!_instance)
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
            Destroy(gameObject);

        if (gameObject != null)
            gameObject.SetActive(false);
    }
}
