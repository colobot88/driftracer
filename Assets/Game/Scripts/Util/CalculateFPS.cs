﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CalculateFPS : MonoBehaviour {

	public Text fpsText;
	double frameCount = 0;
	double dt = 0.0f;
	double fps = 0.0f;
	double updateRate = 4.0f;  // 4 updates per sec.

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		frameCount++;
		dt += Time.deltaTime;
		if (dt > 1.0/updateRate)
		{
			fps = frameCount / dt ;
			frameCount = 0;
			dt -= 1.0/updateRate;
		}
		fpsText.text = fps.ToString ();
	}
}
