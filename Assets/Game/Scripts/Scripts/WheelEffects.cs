using System.Collections;
using UnityEngine;

namespace UnityStandardAssets.Vehicles.Car
{
    [RequireComponent(typeof (AudioSource))]
    public class WheelEffects : MonoBehaviour
    {
        public Transform SkidTrailPrefab;
        public static Transform skidTrailsDetachedParent;
        public ParticleSystem skidParticles;
        public bool skidding { get; private set; }
        public bool PlayingAudio { get; private set; }
        
        private AudioSource m_AudioSource;
        private Transform m_SkidTrail;
        private WheelCollider m_WheelCollider;
        private ParticleSystem.EmissionModule emission;
        private float wheelSlipAmountSideways;

        private void Start()
        {
            //skidParticles = transform.root.GetComponentInChildren<ParticleSystem>();

            if (skidParticles == null)
            {
                Debug.LogWarning(" no particle system found on car to generate smoke particles");
            }
            else
            {
                skidParticles.Stop();
                emission = skidParticles.emission;
                emission.enabled = false;
            }

            m_WheelCollider = GetComponent<WheelCollider>();
            m_AudioSource = GetComponent<AudioSource>();
            PlayingAudio = false;

            GameObject skidParent = GameObject.Find("Skid Trails - Detached");
            if (skidParent == null)
                skidTrailsDetachedParent = null;
            if (skidTrailsDetachedParent == null)
            {
                skidTrailsDetachedParent = new GameObject("Skid Trails - Detached").transform;
            }
        }

        void Update()
        {
            //if (GameState.CurrentGameState == GAMESTATES.PLAYBACK)
            //    emission.enabled = false;
        }

        public void EmitTyreSmoke()
        {
            if (GameManager.Instance != null && Settings.ParticlesEnabled)
            {
                emission.enabled = true;
                skidParticles.transform.position = transform.position - Vector3.up * m_WheelCollider.radius;
                skidParticles.Emit(1);
            }
            if (!skidding)
            {
                StartCoroutine(StartSkidTrail());
            }
        }


        public void PlayAudio()
        {
            m_AudioSource.Play();
            PlayingAudio = true;
        }


        public void StopAudio()
        {
            m_AudioSource.Stop();
            PlayingAudio = false;
        }


        public IEnumerator StartSkidTrail()
        {
            skidding = true;
            m_SkidTrail = Instantiate(SkidTrailPrefab);
            while (m_SkidTrail == null)
            {
                yield return null;
            }
            m_SkidTrail.parent = transform;
            m_SkidTrail.localPosition = -Vector3.up*m_WheelCollider.radius;
        }

        public void Reset()
        {
            EndSkidTrail();
            if(m_SkidTrail != null)
                Destroy(m_SkidTrail.gameObject);
        }

        public void EndSkidTrail()
        {
            emission.enabled = false;
            if (!skidding)
            {
                return;
            }
            skidding = false;
            if (skidTrailsDetachedParent == null)
                skidTrailsDetachedParent = null;
            if (skidTrailsDetachedParent == null)
            {
                skidTrailsDetachedParent = new GameObject("Skid Trails - Detached").transform;
            }
            m_SkidTrail.parent = skidTrailsDetachedParent;
            Destroy(m_SkidTrail.gameObject, 1);
        }
    }
}
