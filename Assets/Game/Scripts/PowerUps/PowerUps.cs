﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Vehicles.Car;

public class PowerUps : MonoBehaviour {

    private UIController uiController;

    private CarController carController;

    private PowerUp nitrous;
    private bool usingNitrous = false;
    public GameObject nitrousParent;
    public float nitrousCapacity = 0;
    public float nitrousMultiplier = 0;
    public float usageMultiplier = 1f;

    public PowerUp Nitrous
    {
        get
        {
            return nitrous;
        }

        set
        {
            nitrous = value;
        }
    }

    void Awake()
    {
        //carController = GetComponent<CarController>();

        //nitrous = new Nitrous(carController, nitrousCapacity, nitrousMultiplier, usageMultiplier);
        //uiController = GameObject.FindObjectOfType<UIController>();
        //uiController.OnNitrousClicked += UiController_OnNitrousClicked;
        //nitrous.OnValueChange += uiController.UpdateNitrous;
        
    }

    public void Initialize()
    {
        carController = GetComponent<CarController>();

        Nitrous = new Nitrous(carController, nitrousCapacity, nitrousMultiplier, usageMultiplier);
        if (nitrousParent != null)
            Nitrous.SetEffectObject(nitrousParent);
        uiController = GameObject.FindObjectOfType<UIController>();
        uiController.OnNitrousClicked += UiController_OnNitrousClicked;
        Nitrous.OnValueChange += uiController.UpdateNitrous;
    }

    private void UiController_OnNitrousClicked(bool active)
    {
        usingNitrous = active;
        Nitrous.StartStop(active);
    }

    // Use this for initialization
    void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
	    if(usingNitrous)
        {
            float leftAmount = Nitrous.Use(Time.deltaTime);
        }
    }
}
