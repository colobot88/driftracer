﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Vehicles.Car;

public abstract class PowerUp{

    CarController carController;
    float capacity;
    float amount;
    float multiplier;
    float usageMultiplier;
    bool isActive = false;

    public delegate void OnValueChangeEvent(float leftAmount);
    public event OnValueChangeEvent OnValueChange;

    public float Capacity
    {
        get
        {
            return capacity;
        }

        set
        {
            capacity = value;
        }
    }

    public float Amount
    {
        get
        {
            return amount;
        }

        set
        {
            amount = value;
            if (amount < 0)
            {
                amount = 0;
                StartStop(false);
            }
            else if (amount > Capacity)
                amount = Capacity;
            if (OnValueChange != null)
                OnValueChange(amount / capacity);
        }
    }

    public float PowerMultiplier
    {
        get
        {
            return multiplier;
        }

        set
        {
            multiplier = value;
        }
    }

    internal abstract void SetEffectObject(GameObject nitrousParent);

    public float UsageMultiplier
    {
        get
        {
            return usageMultiplier;
        }

        set
        {
            usageMultiplier = value;
        }
    }

    public CarController CarController
    {
        get
        {
            return carController;
        }

        set
        {
            carController = value;
        }
    }

    public bool IsActive
    {
        get
        {
            return isActive;
        }

        set
        {
            isActive = value;
        }
    }

    public virtual float Use(float multiplier = 1)
    {
        this.Amount -= UsageMultiplier * multiplier;
        return Amount;
    }

    public void Add(float addAmount)
    {
        this.Amount += addAmount;
    }

    public void Reset()
    {
        this.Amount = this.Capacity;
        StartStop(false);
    }

    public abstract void StartStop(bool active); 
}
