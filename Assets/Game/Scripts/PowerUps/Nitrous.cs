﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Vehicles.Car;
using System;

public class Nitrous : PowerUp{

    public GameObject effectObj;

    public Nitrous(CarController carController, float capacity, float powerMultiplier, float usageMultiplier)
    {
        this.CarController = carController;
        this.Capacity = capacity;
        this.Amount = Capacity;
        this.PowerMultiplier = powerMultiplier;
        this.UsageMultiplier = usageMultiplier;
    }

    public override float Use(float multiplier = 1)
    {
        if (this.IsActive && Amount > 0)
        {
            base.Use(multiplier);
            CarController.GetComponent<Rigidbody>().AddForce(CarController.transform.forward * PowerMultiplier, ForceMode.Acceleration);
            if (effectObj != null && !effectObj.activeInHierarchy & Amount > 0)
                effectObj.SetActive(true);
        }
        return Amount;
    }

    public override void StartStop(bool active)
    {
        if (active)
        {
            if (Amount > 0)
            {
                this.IsActive = active; 
            }
        }
        else if (IsActive)
        {
            this.IsActive = active;
            if (effectObj != null)
                effectObj.SetActive(false);
        }
    }

    internal override void SetEffectObject(GameObject nitrousParent)
    {
        this.effectObj = nitrousParent;
    }
}
