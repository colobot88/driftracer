﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class Language {

    private static int langIndex = 0;
    private static List<Languages> languages = new List<Languages>() {Languages.TR, Languages.EN };
    private static Languages currentLanguage = Languages.EN;

    //events
    public delegate void OnLanguageChangedEvent(Languages current);
    public static event OnLanguageChangedEvent OnLanguageChanged;

    public static Dictionary<LanguageKeys, string> englishDictionary = new Dictionary<LanguageKeys, string>()
    {
        { LanguageKeys.ACCELERATION, "Acceleration" },
        { LanguageKeys.BACK, "Back" },
        { LanguageKeys.BUY, "Buy" },
        { LanguageKeys.CHECK_CONNECTION, "Please check internet connection" },
        { LanguageKeys.CONTINUE, "Continue" },
        { LanguageKeys.DRIFT_TIME, "Drift Time" },
        { LanguageKeys.DISTANCE, "Distance" },
        { LanguageKeys.GAME_OVER, "YOU LOST" },
        { LanguageKeys.GAME_WIN, "YOU WIN" },
        { LanguageKeys.GO_HOME, "Garage" },
        { LanguageKeys.LANGUAGE, "EN" },
        { LanguageKeys.LATER, "Later" },
        { LanguageKeys.LEFT_TIME, "Left Time" },
        { LanguageKeys.LOADING, "LOADING" },
        { LanguageKeys.MAX_SPEED, "Max Speed" },
        { LanguageKeys.MONEY, "Cash" },
        { LanguageKeys.NO, "No" },
        { LanguageKeys.NOT_ENOUGH_MONEY, "Not Enough Money" },
        { LanguageKeys.PLAY, "Play" },
        { LanguageKeys.RATE_US, "Would you like to rate Drift Time?" },
        { LanguageKeys.RESTART, "Restart" },
        { LanguageKeys.RETRIEVE_PRODUCTS, "Retrieving product list. Please wait" },
        { LanguageKeys.SCORE, "Score" },
        { LanguageKeys.TUTORIAL_DRIFT, "Tip: If you drift faster or longer you gain more time and turbo!" },
        { LanguageKeys.TUTORIAL_SLIDER, "Tip: The upper part of the bar on the right is acceleration, the lower part is brake!" },
        { LanguageKeys.TUTORIAL_STEER, "Tip: Don't steer car! It's self-driving! Use gas/brake bar and turbo!" },
        { LanguageKeys.TUTORIAL_TURBO, "Tip: Use turbo in the beginning to gain more time!" },
        { LanguageKeys.VIDEO, "Video" },
        { LanguageKeys.WATCH_TO_END, "Please watch the video until the end" },
        { LanguageKeys.YES, "Yes" }
    };

    public static Dictionary<LanguageKeys, string> turkishDictionary = new Dictionary<LanguageKeys, string>()
    {
        { LanguageKeys.ACCELERATION, "Hızlanma" },
        { LanguageKeys.BACK, "Geri" },
        { LanguageKeys.BUY, "Satın Al" },
        { LanguageKeys.CHECK_CONNECTION, "Lütfen internet bağlantınızı kontrol edin" },
        { LanguageKeys.CONTINUE, "Devam Et" },
        { LanguageKeys.DRIFT_TIME, "Drift Süresi" },
        { LanguageKeys.DISTANCE, "Mesafe" },
        { LanguageKeys.GAME_OVER, "KAYBETTİN" },
        { LanguageKeys.GAME_WIN, "KAZANDIN" },
        { LanguageKeys.GO_HOME, "Garaj" },
        { LanguageKeys.LANGUAGE, "TR" },
        { LanguageKeys.LATER, "Sonra" },
        { LanguageKeys.LEFT_TIME, "Kalan Süre" },
        { LanguageKeys.LOADING, "YÜKLENİYOR" },
        { LanguageKeys.MAX_SPEED, "Max. Hız" },
        { LanguageKeys.MONEY, "Para" },
        { LanguageKeys.NO, "Hayır" },
        { LanguageKeys.NOT_ENOUGH_MONEY, "Yeterli paranız yok" },
        { LanguageKeys.PLAY, "Oyna" },
        { LanguageKeys.RATE_US, "Drift Time'ı yorumlayarak destek olmak ister misin?" },
        { LanguageKeys.RESTART, "Yeniden Başla" },
        { LanguageKeys.RETRIEVE_PRODUCTS, "Ürün listesi alınıyor. Lütfen Bekleyin." },
        { LanguageKeys.SCORE, "Skor" },
        { LanguageKeys.TUTORIAL_DRIFT, "İpucu: Ne kadar hızlı veya uzun drift yaparsan o kadar süre kazanırsın!" },
        { LanguageKeys.TUTORIAL_SLIDER, "İpucu: Ekranın sağındaki çubuğun üst kısmı gaz, alt kısmı frendir!" },
        { LanguageKeys.TUTORIAL_STEER, "İpucu: Arabayı yönlendirmeye çalışma! Araba yolu takip edebilir! Sadece gaz/fren çubuğunu ve turboyu kullan!" },
        { LanguageKeys.TUTORIAL_TURBO, "İpucu: Oyun başında turbo kullanmak daha çok süre kazanmanı sağlayabilir!" },
        { LanguageKeys.VIDEO, "Video" },
        { LanguageKeys.WATCH_TO_END, "Devam etmek için videoyu sonuna kadar izleyin" },
        { LanguageKeys.YES, "Evet" },
    };

    public static Languages CurrentLanguage
    {
        get
        {
            return currentLanguage;
        }

        set
        {
            currentLanguage = value;

            if (OnLanguageChanged != null)
                OnLanguageChanged(currentLanguage);
        }
    }

    public static int LangIndex
    {
        get
        {
            return langIndex;
        }

        set
        {
            langIndex = value;
        }
    }

    public static Dictionary<LanguageKeys, string> GetLanguageDictionary(Languages currentLanguage)
    {
        if (currentLanguage == Languages.EN)
            return englishDictionary;
        else if (currentLanguage == Languages.TR)
            return turkishDictionary;
        return null;
    }

    public static Languages GetNextLanguage()
    {
        LangIndex++;
        if (LangIndex >= languages.Count)
            LangIndex = 0;

        return languages[LangIndex];
    }

    public static void ChangeLanguage()
    {
        CurrentLanguage = GetNextLanguage();
    }

    public static string GetTip()
    {
        float random = UnityEngine.Random.Range(0, 4);

        if(random < 1)
        {
            return GetLanguageDictionary(Language.CurrentLanguage)[LanguageKeys.TUTORIAL_DRIFT];
        } else if(random < 2)
        {
            return GetLanguageDictionary(Language.CurrentLanguage)[LanguageKeys.TUTORIAL_SLIDER];
        }
        else if (random < 3)
        {
            return GetLanguageDictionary(Language.CurrentLanguage)[LanguageKeys.TUTORIAL_TURBO];
        }
        else if (random < 4)
        {
            return GetLanguageDictionary(Language.CurrentLanguage)[LanguageKeys.TUTORIAL_STEER];
        }
        return "";
    }
}

public enum Languages
{
    TR,
    EN,
    FR,
    IT
}

public enum LanguageKeys
{
    ACCELERATION,
    BACK,
    BUY,
    CHECK_CONNECTION,
    CONTINUE,
    DISTANCE,
    DRIFT_TIME,
    GAME_OVER,
    GAME_WIN,
    GO_HOME,
    LANGUAGE,
    LEFT_TIME,
    LOADING,
    LATER,
    MAX_SPEED,
    MONEY,
    NO,
    NOT_ENOUGH_MONEY,
    PLAY,
    RATE_US,
    RESTART,
    RETRIEVE_PRODUCTS,
    SCORE,
    TUTORIAL_DRIFT,
    TUTORIAL_SLIDER,
    TUTORIAL_STEER,
    TUTORIAL_TURBO,
    VIDEO,
    WATCH_TO_END,
    YES
}
