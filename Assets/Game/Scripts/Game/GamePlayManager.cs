﻿using System.Collections;
using UnityEngine;

public class GamePlayManager : MonoBehaviour
{
    #region GamePlayManager Instance
    private static GamePlayManager _instance = null; //singleton instance
    public static GamePlayManager Instance
    {
        get
        {
            return _instance;
        }
    }
    #endregion

    #region Gameplay Helper Managers
    private GameState gameState;
    private GamePlayTimeManager timeManager;
    public PlaybackManager playbackManager;
    public RoadManager roadManager;
    public CarManager carManager;
    public WeatherManager weatherManager;
    #endregion
    #region Gameplay Helper Manager Properties
    public GamePlayTimeManager TimeManager
    {
        get
        {
            return timeManager;
        }
        set
        {
            timeManager = value;
        }
    }

    public PlaybackManager PlaybackManager
    {
        get
        {
            return playbackManager;
        }

        set
        {
            playbackManager = value;
        }
    }

    public RoadManager RoadManager
    {
        get
        {
            return roadManager;
        }

        set
        {
            roadManager = value;
        }
    }

    public CarManager CarManager
    {
        get
        {
            return carManager;
        }

        set
        {
            carManager = value;
        }
    }

    public GameState GameState
    {
        get
        {
            return gameState;
        }

        set
        {
            gameState = value;
        }
    }
    #endregion

    #region Initialization
    void Awake()
    {
        if (!_instance)
            _instance = this;
        GameState = new GameState();
        TimeManager = new GamePlayTimeManager();
    }

    void Start()
    {
        GameState.OnGameStateChanged += GameState_OnGameStateChanged;
        PlaybackManager.Reset();
        roadManager.Reset();
        weatherManager.Reset();
        carManager.InitializeManager(roadManager, weatherManager);
        TimeManager.Reset();
        GameState.CurrentGameState = GAMESTATES.NOT_STARTED;
    }
    #endregion
    
    void Update()
    {
        if (GamePlayManager.Instance.GameState.CurrentGameState == GAMESTATES.PLAYING)
        {
            TimeManager.UpdateTimeManager(Time.deltaTime);
            if (Time.deltaTime > 0)
                PlaybackManager.AddPlaybackData(Time.deltaTime, carManager.car.transform.position, carManager.car.transform.rotation, carManager.carRB.velocity, carManager.carRB.angularVelocity, carManager.wpTracker.progressNum, TimeManager.TimeLeft, carManager.TotalDistance, carManager.PowerUps.Nitrous.Amount, CarManager.TotalDriftTime, CarManager.LiveScore);
        }
    }

    #region Methods - called when game state change
    private void GameState_OnGameStateChanged(GAMESTATES gs)
    {
        if (gs == GAMESTATES.WRONG_WAY || gs == GAMESTATES.TIME_OVER || gs == GAMESTATES.OFF_ROAD)
            StartSlowDown();
        if(gs == GAMESTATES.FINISH)
            StartGameFinish();
    }

    public void OnPlaybackClicked()
    {
        StopAllCoroutines();

        carManager.OnPlaybackClicked();
        PlaybackManager.StartPlayback(carManager.car);
    }

    public void OnRestartClicked()
    {
        StopAllCoroutines();

        RoadManager.Reset();
        PlaybackManager.Reset();
        carManager.Reset();
        TimeManager.Reset();
        GamePlayManager.Instance.GameState.CurrentGameState = GAMESTATES.NOT_STARTED;
    }
    #endregion

    #region SlowDownGame
    public void StartGameFinish()
    {
        if (Time.timeScale >= 1)
        {
            carManager.SetCarSoundOnOff(false);
            //Time.timeScale = 0.25f;
            StartCoroutine(waitForSecond(3));
            //StartCoroutine(SlowDown()); // just for bullet time
        }
    }

    public void StartSlowDown(bool canContinue = true)
    {
        if(Time.timeScale >= 1)
        {
            carManager.SetCarSoundOnOff(false);
            Time.timeScale = 0.05f;
            StartCoroutine(SlowDown()); // just for bullet time
        }
    }

    private IEnumerator SlowDown() // just for bullet time
    {
        while (Time.timeScale >= 0)
        {
            if (Time.timeScale > 0f)
                Time.timeScale -= .01f;
            if (Time.timeScale < 0f)
                Time.timeScale = 0f;
            yield return new WaitForSeconds(.1f);
        }
    }
    private IEnumerator waitForSecond(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        gameState.CurrentGameState = GAMESTATES.FINISH_GAMEOVER;
    }
    #endregion

    private GameObject uniStormSystem;
    private UniStormWeatherSystem_C uniStormSystemScript;

    public void ChangeWeather()
    {
        //Find the UniStorm Weather System Editor, this must match the UniStorm Editor name
        uniStormSystem = GameObject.Find("UniStormSystemEditor");
        uniStormSystemScript = uniStormSystem.GetComponent<UniStormWeatherSystem_C>();
        uniStormSystemScript.weatherForecaster = Random.Range(1, 10);
    }
}
