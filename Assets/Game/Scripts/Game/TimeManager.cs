﻿using UnityEngine;
using System.Collections;

public class GamePlayTimeManager {

    #region Time vars
    private float timeLeft = 20f; //if under 0 then gameover..
    private float timePassed = 0f; //time since game started..
    #endregion

    #region Time events
    //public delegate void OnTimeOverEvent(bool timeOver);
    //public event OnTimeOverEvent OnTimeOver;

    public delegate void OnTimeLeftChangedEvent(float timeLeft);
    public event OnTimeLeftChangedEvent OnTimeLeftChanged;
    #endregion

    public GamePlayTimeManager()
    {
    }

    public void UpdateTimeManager(float deltaTime)
    {
        TimePassed += deltaTime; //TODO: decrease on playback
        this.TimeLeft -= deltaTime;
    }

    #region Properties
    public float TimeLeft
    {
        get
        {
            return timeLeft;
        }

        set
        {
            timeLeft = value;
            if (OnTimeLeftChanged != null)
                OnTimeLeftChanged(timeLeft);
            if (timeLeft < 0)
            {
                timeLeft = 0f;
                GamePlayManager.Instance.GameState.CurrentGameState = GAMESTATES.TIME_OVER;
            }
        }
    }

    public float TimePassed
    {
        get
        {
            return timePassed;
        }

        set
        {
            timePassed = value;
        }
    }
    #endregion

    #region public methods
    public void IncreaseTimeLeft(float inceTime) //increase timeleft value while car drifting
    {
        this.TimeLeft += inceTime;
    }

    public void SetTimeLeft(float time) //playback manager sets timeleft while playbacking
    {
        this.TimeLeft = time;
    }

    public void Reset() //reset time variables when game restarted
    {
        TimeLeft = 20.0f;
        TimePassed = 0f;
    }
    #endregion
}
