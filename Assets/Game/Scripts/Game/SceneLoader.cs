﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    private bool isSceneLoading = false; //flag to check if scene is loading

    public GameObject loadingCanvas; //loading canvas to display while loading
    public Text loadingText;
    public Text tipText;

    // Updates once per frame
    void Update()
    {
        // If scene has started loading
        if (isSceneLoading == true)
        {
            //animate "loading text" while loading scene
            loadingText.color = new Color(loadingText.color.r, loadingText.color.g, loadingText.color.b, Mathf.PingPong(Time.time, 1));
        }
    }

    //start loading scene with sceneNo
    public void LoadScene(int sceneNo)
    {
        loadingText.text = Language.GetLanguageDictionary(Language.CurrentLanguage)[LanguageKeys.LOADING] + "...";
        tipText.text = Language.GetTip();
        loadingCanvas.SetActive(true);
        isSceneLoading = true;
        StartCoroutine(LoadNewScene(sceneNo));
    }

    // The coroutine runs on its own at the same time as Update() and takes an integer indicating which scene to load.
    IEnumerator LoadNewScene(int sceneNo)
    {
        // Start an asynchronous operation to load the scene that was passed to the LoadNewScene coroutine.
        AsyncOperation async = SceneManager.LoadSceneAsync(sceneNo);

        // While the asynchronous operation to load the new scene is not yet complete, continue waiting until it's done.
        while (!async.isDone)
        {
            yield return null;
        }

        StartCoroutine(CloseSceneLoader());

        //isSceneLoading = false;
        //loadingCanvas.SetActive(false);
    }

    IEnumerator CloseSceneLoader()
    {
        yield return new WaitForSeconds(2);
        isSceneLoading = false;
        loadingCanvas.SetActive(false);
    }

    public void Close()
    {
        if(!isSceneLoading)
        {
            gameObject.SetActive(false);
        }
    }
}