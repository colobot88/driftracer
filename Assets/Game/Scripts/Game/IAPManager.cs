﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class IAPManager : MonoBehaviour {

    public Purchaser purchaser;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void SetActive(bool flag)
    {
        gameObject.SetActive(flag);
        purchaser.OpenClose(true);
    }
}
