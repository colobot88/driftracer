﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.Utility;

public class Road : MonoBehaviour {

	public WaypointCircuit wpCircuit = null;
	public Transform initialPositionRefTransform;
    public Transform[] firstWaypoints;

	void Awake()
	{

	}

	// Use this for initialization
	void Start () {
        Mesh mesh = GetComponent<MeshFilter>().mesh;
        mesh.Optimize();
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Initialize()
    {
        foreach (Transform wpTr in firstWaypoints)
            wpTr.position = firstWaypoints[firstWaypoints.Length - 1].position;
    }
}
