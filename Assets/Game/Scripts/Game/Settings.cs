﻿public static class Settings{

    public delegate void OnQualityChangedEvent(GraphicsQuality quality);
    public static event OnQualityChangedEvent OnQualityChanged;

    private static bool particlesEnabled = false;

    public static bool ParticlesEnabled
    {
        get
        {
            return particlesEnabled;
        }

        set
        {
            particlesEnabled = value;
        }
    }

    public static GraphicsQuality Quality
    {
        get
        {
            return quality;
        }

        set
        {
            quality = value;
        }
    }

    private static GraphicsQuality quality = GraphicsQuality.LOW;
    public static void ChangeGraphicsQuality()
    {
        if (Quality == GraphicsQuality.HIGH)
            Quality = GraphicsQuality.LOW;
        else
            Quality++;
        if (OnQualityChanged != null)
            OnQualityChanged(Quality);
    }
}

public enum GraphicsQuality
{
    LOW,
    MED,
    HIGH
}