﻿using UnityEngine;

public class GameState {

    private GAMESTATES gameState = GAMESTATES.NOT_STARTED;

    // Events
    public delegate void OnGameStateChangedEvent(GAMESTATES gs);
    public event OnGameStateChangedEvent OnGameStateChanged;

    public GameState()
    {
        gameState = GAMESTATES.NOT_STARTED;
    }

    public GAMESTATES CurrentGameState
    {
        get
        {
            return gameState;
        }
        set
        {
            if (gameState != value)
            {
                gameState = value;
                if (gameState == GAMESTATES.NOT_STARTED || gameState == GAMESTATES.PLAYING || gameState == GAMESTATES.PLAYBACK)
                    Time.timeScale = 1;
                if (OnGameStateChanged != null)
                    OnGameStateChanged(gameState);
            }
        }
    }
}

public enum GAMESTATES
{
    NOT_STARTED,
    PLAYBACK,
    PLAYING,
    TIME_OVER,
    OFF_ROAD,
    WRONG_WAY,
    SLOW_DOWN,
    FINISH,
    FINISH_GAMEOVER
}
