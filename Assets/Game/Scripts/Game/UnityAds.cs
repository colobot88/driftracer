﻿using UnityEngine;
using System.Collections;
using UnityEngine.Advertisements;
using UnityEngine.Analytics;
using System.Collections.Generic;

public class UnityAds : MonoBehaviour
{
    private static UnityAds _instance = null;

    private bool unityAdsInitialized = false;//can be used for informing the user about the status

    public string zoneId;
    private int reward = 0;
    public bool isRewarded = false;

    public delegate void OnAdWatchedEvent(bool success);
    public event OnAdWatchedEvent OnAdWatched;

    public delegate void OnMoneyAdWatchedEvent(int reward);
    public event OnMoneyAdWatchedEvent OnMoneyAdWatched;

    // Serialize private fields
    //  instead of making them public.
    [SerializeField]
    string iosGameId;
    [SerializeField]
    string androidGameId;
    [SerializeField]
    bool enableTestMode;

    void Awake()
    {
        if (!_instance)
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
            Destroy(gameObject); 
    }

    void Start()
    {
        StartCoroutine(checkInternetConnection());
//        string gameId = null;
//        if (string.IsNullOrEmpty(zoneId)) zoneId = null;

//#if UNITY_IOS // If build platform is set to iOS...
//        gameId = iosGameId;
//#elif UNITY_ANDROID // Else if build platform is set to Android...
//        gameId = androidGameId;
//#endif

//        if (string.IsNullOrEmpty(gameId))
//        { // Make sure the Game ID is set.
//            Debug.LogError("Failed to initialize Unity Ads. Game ID is null or empty.");
//        }
//        else if (!Advertisement.isSupported)
//        {
//            Debug.LogWarning("Unable to initialize Unity Ads. Platform not supported.");
//        }
//        else if (Advertisement.isInitialized)
//        {
//            Debug.Log("Unity Ads is already initialized.");
//        }
//        else
//        {
//            Debug.Log(string.Format("Initialize Unity Ads using Game ID {0} with Test Mode {1}.",
//                gameId, enableTestMode ? "enabled" : "disabled"));
//            Advertisement.Initialize(gameId, enableTestMode);
//        }
    }

    public void DisplayMoneyAwardedAd(int _reward)
    {
        Debug.Log("DisplayMoneyAwardedAd");
        reward = _reward;
        if (Advertisement.IsReady())
            Advertisement.IsReady(zoneId);

        ShowOptions options = new ShowOptions();
        options.resultCallback = MoneyHandleShowResult;

        Analytics.CustomEvent("Ad_Started", new Dictionary<string, object>
                    {
                        { "Ad_Started", "true"}
                    });

        Advertisement.Show(zoneId, options);
    }

    private void MoneyHandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                isRewarded = true;
                Debug.Log("Money Video completed. User rewarded " + isRewarded + " credits.");
                if (OnMoneyAdWatched != null)
                {
                    OnMoneyAdWatched(reward);
                    Analytics.CustomEvent("Ad_Completed", new Dictionary<string, object>
                    {
                        { "Ad_Completed", "true"}
                    });
                }
                break;
            case ShowResult.Skipped:
                Debug.Log("Video was skipped.");
                if (OnMoneyAdWatched != null)
                    OnMoneyAdWatched(0);
                break;
            case ShowResult.Failed:
                Debug.Log("Video failed to show.");
                if (OnMoneyAdWatched != null)
                    OnMoneyAdWatched(0);
                break;
        }
    }

    public void DisplayAwardedAd()
    {
        Debug.Log("DisplayAwardedAd");
        isRewarded = false;
        if (Advertisement.IsReady())
            Advertisement.IsReady(zoneId);

        ShowOptions options = new ShowOptions();
        options.resultCallback = HandleShowResult;

        Analytics.CustomEvent("Ad_Started", new Dictionary<string, object>
                    {
                        { "Ad_Started", "true"}
                    });

        Advertisement.Show(zoneId, options);
    }

    private void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                isRewarded = true;
                Debug.Log("Video completed. User rewarded " + isRewarded + " credits.");
                if (OnAdWatched != null)
                {
                    OnAdWatched(true);
                    
                    Analytics.CustomEvent("Ad_Completed", new Dictionary<string, object>
                    {
                        { "Ad_Completed", "true"}
                    });
                }
                break;
            case ShowResult.Skipped:
                Debug.Log("Video was skipped.");
                if (OnAdWatched != null)
                    OnAdWatched(false);
                break;
            case ShowResult.Failed:
                Debug.Log("Video failed to show.");
                if (OnAdWatched != null)
                    OnAdWatched(false);
                break;
        }
    }

    public IEnumerator checkInternetConnection()
    {
        while (!unityAdsInitialized)
        {
            WWW www = new WWW("http://google.com");
            yield return www;
            Debug.Log("checking connection...");
            if (www.error == null)
            {
                Initialize();

                GameManager.Instance.connectedInternet = true;

                break;//will end the coroutine
            }

            for (int i = 0; i < 100; i++)
            {
                yield return null;
            }
        }
    }

    public void Initialize()
    {
        string gameId = null;
        if (string.IsNullOrEmpty(zoneId)) zoneId = null;

#if UNITY_IOS // If build platform is set to iOS...
                gameId = iosGameId;
#elif UNITY_ANDROID // Else if build platform is set to Android...
        gameId = androidGameId;
#endif

        if (string.IsNullOrEmpty(gameId))
        { // Make sure the Game ID is set.
            Debug.LogError("Failed to initialize Unity Ads. Game ID is null or empty.");
        }
        else if (!Advertisement.isSupported)
        {
            Debug.LogWarning("Unable to initialize Unity Ads. Platform not supported.");
        }
        else if (Advertisement.isInitialized)
        {
            Debug.Log("Unity Ads is already initialized.");
        }
        else
        {
            Debug.Log(string.Format("Initialize Unity Ads using Game ID {0} with Test Mode {1}.",
                gameId, enableTestMode ? "enabled" : "disabled"));
            Advertisement.Initialize(gameId, enableTestMode);
        }
    }
}