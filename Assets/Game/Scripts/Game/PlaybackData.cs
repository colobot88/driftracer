﻿using UnityEngine;
using System.Collections;

public struct PlaybackData {

    public Vector3 position;
    public Quaternion rotation;
    public Vector3 velocity;
    public Vector3 angularVelocity;
    public int wpProgressNum;
    public float timeLeft;
    public float totalDistance;
    public float nitrousAmount;
    public float driftTime;
    public float score;

    public PlaybackData(Vector3 pos, Quaternion rot, Vector3 vel, Vector3 angVel, int progressNum, float tLeft, float distance, float nitroAmount, float totalDriftTime, float scr)
    {
        position = pos;
        rotation = rot;
        velocity = vel;
        angularVelocity = angVel;
        wpProgressNum = progressNum;
        timeLeft = tLeft;
        totalDistance = distance;
        nitrousAmount = nitroAmount;
        driftTime = totalDriftTime;
        score = scr;
    }
}
