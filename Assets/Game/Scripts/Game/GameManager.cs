﻿using System.Collections;
using UnityEngine;

/// <summary>
/// GameManager is the main class of this project.
/// GameManager changes scenes via sceneloader
/// GameManager enable/disable music for all scenes
/// GameManager moves the selected car from modification scene to gameplay scene
/// </summary>
public class GameManager : MonoBehaviour {

    #region GameManager Instance
    private static GameManager _instance = null; //singleton instance
    public static GameManager Instance
    {
        get
        {
            return _instance;
        }
    }
    #endregion

    #region Private Vars

    //Data Variables
    private IDataManager dataManager; //DataManager -> save/loads game/player data

    private PurchaseManager purchaseManager;

    //Game Variables
    private GameObject currentCar; //current car object : to move between scenes
    #endregion

    #region Public Vars
    //SceneLoader
    public SceneLoader sceneLoader; //reference this on editor (child of gamemanager since we need this any scene)

    //Ad Manager
    public UnityAds adsManager; //reference this on editor (child of gamemanager since we need this any scene)
    
    //Audio Variables
    public AudioSource music; //reference this on editor (child of gamemanager since we need this any scene)
    #endregion

    public bool askRate = false;
    public bool connectedInternet = false;

    #region Initialization
    void Awake()
    {
        if (!_instance)
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);

            //initialize Settings
            //Settings = new Settings();

            //initilize DataManager
            dataManager = global::DataManager.Instance;
            DataManager.Initialize();

            Language.CurrentLanguage = (Languages)DataManager.PlayerData.CurrentLanguage;

            //Language.LangIndex = (int)Language.CurrentLanguage;


            PurchaseManager = new PurchaseManager();
            Debug.Log("PurchaseManager  " + PurchaseManager);
            adsManager.OnMoneyAdWatched += DataManager.AdsManager_OnMoneyAdWatched;
        }
        else
            Destroy(gameObject);
    }

    void Start () {
        Application.targetFrameRate = 60;
    }
    #endregion

    #region DataManagement
    public IDataManager DataManager
    {
        get
        {
            return dataManager;
        }
    }

    internal PurchaseManager PurchaseManager
    {
        get
        {
            return purchaseManager;
        }

        set
        {
            purchaseManager = value;
        }
    }
    #endregion

    #region Car
    public GameObject GetCurrentCarObj()
    {
        Destroy(currentCar.GetComponent<CamRotater>());
        return this.currentCar;
    }

    public void SetCurrentCarObj(GameObject car)
    {
        car.transform.parent = transform;
        this.currentCar = car;
    }
    #endregion

    #region Music
    public void RestartMusic()
    {
        music.enabled = false;
        music.enabled = true;
    }
    public void PlayMusic(bool play)
    {
        music.volume = play ? 1 : 0;
    }
    public void StopMusic()
    {
        music.enabled = false;
    }
    public bool isMusicPlaying()
    {
        return music.volume == 1;
    }
    #endregion

    #region Scene
    //Scene methods
    public void GoMainScreen()
    {
        Destroy(currentCar);
        sceneLoader.LoadScene(0);
    }

    public void LoadScene(int sceneNo)
    {
        sceneLoader.LoadScene(sceneNo);
    }
    #endregion

    public void RateClicked()
    {
        //Debug.Log("RateClicked");
        //StartCoroutine(waitForSecond5000CASH(3));
    }

    private IEnumerator waitForSecond5000CASH(float waitTime)
    {
        Debug.Log("waitForSecond5000CASH");
        yield return new WaitForSeconds(waitTime);
        Debug.Log("waitTime");
        GameManager.Instance.DataManager.PlayerData.Rated = true;
        GameManager.Instance.DataManager.EarnMoney(5000);
        GameManager.Instance.DataManager.SavePlayerData();
    }
}
