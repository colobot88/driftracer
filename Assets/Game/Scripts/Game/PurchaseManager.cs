﻿internal class PurchaseManager
{
    public PurchaseManager()
    {

    }

    public bool BuyWithCost(int cost)
    {
        if (GameManager.Instance.DataManager.PlayerData.TotalMoney >= cost)
        {
            GameManager.Instance.DataManager.PurchaseItem(cost);
            return true;
        }
        else
            return false;
    }
}