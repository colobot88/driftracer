﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Utility;

/// <summary>
/// This class handles playback actions.
/// Class has "AddPlaybackData(...)" method which gets data and save it in "playbackDataList" for "saveForSeconds".
/// When "Playback" method is called, a copy of "playbackDataList" is taken.
/// When "Playback" method is called, All the related variables are set based on "playbackDataList"
/// When "Playback" method is called, if playbackTime less than saveForSeconds then "copyLastPlaybackData" is used to playback
/// </summary>
public class PlaybackManager : MonoBehaviour
{
    #region PlaybackManager Instance
    private static PlaybackManager _instance = null;

    public static PlaybackManager Instance
    {
        get
        {
            return _instance;
        }
    }
    #endregion

    #region Initialization
    void Awake()
    {
        if (!_instance)
        {
            _instance = this;
        }
    }
    #endregion

    #region Playback Variables
    private bool playbackReady = false;
    private List<PlaybackData> playbackDataList = new List<PlaybackData>(); //playback data
    private List<PlaybackData> copyLastPlaybackDataList = new List<PlaybackData>(); //copy of playback data, to use if playback used while new playback data not completed.

    private float saveForSeconds = 5f; // approximately 300 frame but if game runs 30fps then 150. Not works under 30fps
    private int playbackFrameCount = 140;
    private float playbackTime = 0f;
    private static int leftCount = 1;
    #endregion

    #region Playback Events
    public delegate void OnLeftCountChangedEvent(int leftCount);
    public event OnLeftCountChangedEvent OnLeftCountChanged;
    #endregion

    #region Properties
    public int LeftCount
    {
        get
        {
            return leftCount;
        }
        set
        {
            leftCount = value;
            if (OnLeftCountChanged != null)
                OnLeftCountChanged(LeftCount);
        }
    }

    public bool PlaybackReady
    {
        get
        {
            return playbackReady;
        }

        set
        {
            playbackReady = value;
        }
    }
    #endregion

    #region Private Methods
    private void CopyLastPlayback()
    {
        copyLastPlaybackDataList.Clear();
        for (int i = 0; i < playbackDataList.Count; i++)
            copyLastPlaybackDataList.Add(playbackDataList[i]);
    }

    private IEnumerator Playback(GameObject carObj)
    {
        bool isOldPB = true;
        if (!IsOldPlayback())
            isOldPB = false;
        if (LeftCount > 0 || IsOldPlayback())
        {
            if (playbackTime <= saveForSeconds)
            {
                playbackTime = 0f;
                playbackDataList.Clear();
                for (int i = 0; i < copyLastPlaybackDataList.Count; i++)
                    playbackDataList.Add(copyLastPlaybackDataList[i]);
            }
            
            Rigidbody carRB = carObj.GetComponent<Rigidbody>();
            int itemsCount = playbackDataList.Count;
            int playbackCount = itemsCount < playbackFrameCount ? itemsCount : playbackFrameCount;
            WaypointProgressTracker wpTracker = carObj.GetComponent<WaypointProgressTracker>();
            int smallestProgressNum = 9999;
            for (int i = itemsCount - 1; i > itemsCount - playbackCount; i--)
            {
                if (i >= 0)
                {
                    PlaybackData data = playbackDataList[i];
                    carObj.transform.position = data.position;
                    carObj.transform.rotation = data.rotation;
                    carRB.velocity = data.velocity;
                    carRB.angularVelocity = data.angularVelocity;
                    GamePlayManager.Instance.TimeManager.SetTimeLeft(data.timeLeft);
                    GamePlayManager.Instance.CarManager.TotalDistance = data.totalDistance;
                    GamePlayManager.Instance.CarManager.PowerUps.Nitrous.Amount = data.nitrousAmount;
                    GamePlayManager.Instance.CarManager.TotalDriftTime = data.driftTime;
                    GamePlayManager.Instance.carManager.LiveScore = data.score;

                    if (i + 1 < playbackDataList.Count)
                    {
                        if (data.wpProgressNum < smallestProgressNum)
                        {
                            smallestProgressNum = data.wpProgressNum;
                            wpTracker.SetProgressPoint(data.wpProgressNum);
                        }
                    }
                }
                yield return new WaitForSeconds(.01f);
            }
            GamePlayManager.Instance.GameState.CurrentGameState = GAMESTATES.PLAYING;
            if (playbackTime > saveForSeconds)
            {
                CopyLastPlayback();
                playbackTime = 0f;
                playbackDataList.Clear();
            }
        }
        if(!isOldPB)
        {
            LeftCount--;
        }
        yield return 0;
    }
    #endregion

    #region Public Methods
    public void AddPlaybackData(float deltatime, Vector3 position, Quaternion rotation, Vector3 velocity, Vector3 angularVelocity, int progressNum, float timeLeft, float totalDistance, float nitroAmount, float driftTime, float score)
    {
        playbackTime += deltatime;
        playbackDataList.Add(new PlaybackData(position, rotation, velocity, angularVelocity, progressNum, timeLeft, totalDistance, nitroAmount, driftTime, score));

        if (playbackTime > saveForSeconds)
        {
            playbackDataList.RemoveAt(0);
            playbackReady = true;
        }
    }
    public void StartPlayback(GameObject car)
    {
        GamePlayManager.Instance.GameState.CurrentGameState = GAMESTATES.PLAYBACK;
        StartCoroutine(Playback(car));
    }

    public bool IsOldPlayback()
    {
        return playbackTime <= saveForSeconds;
    }

    public void Reset()
    {
        LeftCount = 1;
    }
    #endregion
}
