﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Advertisements;
using UnityStandardAssets.Vehicles.Car;

public class ModificarionUIController : MonoBehaviour {
    
    //events
    public delegate void OnSelectedCarChangedEvent(CarDataMono carData);
    public event OnSelectedCarChangedEvent OnSelectedCarChanged;

    public GameObject modificationPanel;

    public IAPManager iapManager;

    public ModificationManager modificationManager;

    public ColorUIController colorController;
    public RimUIController rimController;

    public InfoPanel infoPanel;

    public Text playerMoney;

    public GameObject soundOn;
    public GameObject soundOff;

    private int carCount = 0;
    private int currentId = 0;

    //buttons
    public GameObject playButton;

    //navigation buttons
    public Button left;
    public Button right;

    //purchase buttons
    public GameObject buyCarButton;
    public Text buyCarText;
    public GameObject buyColorButton;
    public Text buyColorText;
    public GameObject buyRimButton;
    public Text buyRimText;

    //texts
    public Text playText;
    public Text buyText;
    public Text videoText;
    public Image acceleration;
    public Text accelerationText;
    public Image maxspeed;
    public Text maxspeedText;

    //IAP item text
    public Text moneyText1;
    public Text moneyText2;
    public Text moneyText3;
    public Text moneyText4;

    //current selection
    CarDataMono currentCarData;
    int currentColorDataId;
    int currentRimDataId;

    public CarDataMono CurrentCarData
    {
        get
        {
            return currentCarData;
        }

        set
        {
            currentCarData = value;
            if (OnSelectedCarChanged != null)
                OnSelectedCarChanged(currentCarData);
        }
    }


    // Use this for initialization
    void Start () {
        modificationManager = GameObject.FindObjectOfType<ModificationManager>();
        modificationManager.OnSelectedCarChanged += ModificarionUIController_OnSelectedCarChanged;
        modificationManager.OnCarSaved += ModificationManager_OnCarSaved;
        OnSelectedCarChanged += colorController.OnSelectedCarChanged;
        OnSelectedCarChanged += rimController.OnSelectedCarChanged;
        ModificarionUIController_OnSelectedCarChanged(modificationManager.GetCurrentCarController());
        CurrentCarData = modificationManager.GetCurrentCarDataMono();
        colorController.OnSelectedCarChanged(CurrentCarData);
        rimController.OnSelectedCarChanged(CurrentCarData);
        InitializeCarCount(modificationManager.GetTotalCarCount(), modificationManager.GetLastSelectedCarIndex());
        if (GameManager.Instance != null)
        {
            if(!GameManager.Instance.isMusicPlaying())
            {
                ChangeSound(false);
            }
            Language.OnLanguageChanged += Language_OnLanguageChanged;
            Language_OnLanguageChanged(Language.CurrentLanguage); 
            GameManager.Instance.DataManager.OnTotalMoneyChanged += Instance_OnTotalMoneyChanged;
            playerMoney.text = GameManager.Instance.DataManager.PlayerData.TotalMoney.ToString();
        }
        if (!GameManager.Instance.DataManager.PlayerData.Rated)
        {
            if (GameManager.Instance.connectedInternet || Advertisement.IsReady())
            {
                if (GameManager.Instance.askRate)
                {
                    infoPanel.SetText(Language.GetLanguageDictionary(Language.CurrentLanguage)[LanguageKeys.RATE_US]);
                    infoPanel.OpenClosePanel(true, false, true);
                }
            }
        }
	}

    private void ModificationManager_OnCarSaved()
    {
        UpdateCarLocks();
        UpdateColorLocks();
        UpdateRimLocks();
    }

    private void Language_OnLanguageChanged(Languages current)
    {
        playText.text = Language.GetLanguageDictionary(current)[LanguageKeys.PLAY];
        buyText.text = Language.GetLanguageDictionary(current)[LanguageKeys.BUY];
        videoText.text = Language.GetLanguageDictionary(current)[LanguageKeys.VIDEO];
        accelerationText.text = Language.GetLanguageDictionary(current)[LanguageKeys.ACCELERATION];
        maxspeedText.text = Language.GetLanguageDictionary(current)[LanguageKeys.MAX_SPEED];
        moneyText1.text = Language.GetLanguageDictionary(current)[LanguageKeys.MONEY];
        moneyText2.text = Language.GetLanguageDictionary(current)[LanguageKeys.MONEY];
        moneyText3.text = Language.GetLanguageDictionary(current)[LanguageKeys.MONEY];
        moneyText4.text = Language.GetLanguageDictionary(current)[LanguageKeys.MONEY];
    }

    private void ModificarionUIController_OnSelectedCarChanged(CarController carController)
    {
        //TODO
        acceleration.fillAmount = (carController.m_FullTorqueOverAllWheels - 850) / (3100 - 1100);
        maxspeed.fillAmount = (carController.MaxSpeed - 80) / (210 - 80);
    }

    private void Instance_OnTotalMoneyChanged(int totalMoney)
    {
        playerMoney.text = GameManager.Instance.DataManager.PlayerData.TotalMoney.ToString();
    }

    public void InitializeCarCount(int carCount, int currentId)
    {
        this.carCount = carCount;
        this.currentId = currentId;
    }

    public void SetColor(ColorButton colorButton)
    {
        currentColorDataId = colorButton.id;
        modificationManager.ChangeCarColor(colorButton.color, colorButton.id);
        if(CurrentCarData.colorIds.Contains(colorButton.id))
            modificationManager.SaveCarData();
        UpdateColorLocks();
    }

    public void SetRim(int id)
    {
        currentRimDataId = id;
        modificationManager.ChangeCarRim(id);
        if (CurrentCarData.rimIds.Contains(id))
            modificationManager.SaveCarData();
        UpdateRimLocks();
    }

    public void ChangeCar(bool right)
    {
        int nextId = currentId + (right ? 1 : -1);
        if (nextId >= carCount)
            return;
        else if (nextId < 0)
            return;
        currentId = nextId;
        if (currentId <= 0)
        {
            left.interactable = false;
        }
        else
            left.interactable = true;
        if (currentId >= carCount-1)
            this.right.interactable = false;
        else
            this.right.interactable = true;
        UpdateCar();
    }

    private void UpdateCar()
    {
        CurrentCarData = modificationManager.SetCurrentCarByIndex(currentId);//.ModUIController_OnSelectedCarChanged(nextId);
        currentColorDataId = currentCarData.currentColorId;
        currentRimDataId = currentCarData.RimId;
        UpdateCarLocks();
        if (GameManager.Instance.DataManager.PlayerData.CarIds.Contains(CurrentCarData.uuid))
        {
            UpdateColorLocks();
            UpdateRimLocks();
        }
    }

    private void UpdateRimLocks()
    {
        Debug.Log("UpdateRimLocks");
        if (CurrentCarData.rimIds.Contains(currentRimDataId))
        {
            playButton.SetActive(true);
            buyRimButton.SetActive(false);
        }
        else
        {
            playButton.SetActive(false);
            buyRimButton.SetActive(true);
            buyRimText.text = rimController.GetRimDataById(currentRimDataId).cost.ToString();
        }
        rimController.OnSelectedCarChanged(CurrentCarData);
    }

    private void UpdateColorLocks()
    {
        Debug.Log("UpdateColorLocks");
        if (CurrentCarData.colorIds.Contains(currentColorDataId))
        {
            playButton.SetActive(true);
            buyColorButton.SetActive(false);
        }
        else
        {
            playButton.SetActive(false);
            buyColorButton.SetActive(true);
            buyColorText.text = colorController.GetColorDataById(currentColorDataId).cost.ToString();
        }
        colorController.OnSelectedCarChanged(CurrentCarData);
    }

    private void UpdateCarLocks()
    {
        if (GameManager.Instance.DataManager.PlayerData.CarIds.Contains(CurrentCarData.uuid))
        {
            playButton.SetActive(true);
            buyCarButton.SetActive(false);
            modificationPanel.SetActive(true);
        }
        else
        {
            modificationPanel.SetActive(false);
            playButton.SetActive(false);
            buyCarButton.SetActive(true);
            buyColorButton.SetActive(false);
            buyRimButton.SetActive(false);
            buyCarText.text = CurrentCarData.cost.ToString();
        }
    }

    public void Play(int sceneNo)
    {
        //unregister events
        modificationManager.OnSelectedCarChanged -= ModificarionUIController_OnSelectedCarChanged;
        modificationManager.OnCarSaved -= ModificationManager_OnCarSaved;
        OnSelectedCarChanged = colorController.OnSelectedCarChanged;
        OnSelectedCarChanged = rimController.OnSelectedCarChanged;
        Language.OnLanguageChanged -= Language_OnLanguageChanged;
        GameManager.Instance.DataManager.OnTotalMoneyChanged -= Instance_OnTotalMoneyChanged;

        //Stop music
        GameManager.Instance.StopMusic();
        //GameManager.Instance.PlayMusic(false);

        modificationManager.SaveCarData();
        
        GameManager.Instance.LoadScene(sceneNo);
    }

    public void OpenIAPPanel(bool open)
    {
        infoPanel.OpenClosePanel(false);
        iapManager.SetActive(open);
    }

    public void ShowVideo()
    {
        infoPanel.OpenClosePanel(false);
        if (Advertisement.IsReady())
        {
            GameManager.Instance.adsManager.DisplayMoneyAwardedAd(500);
        }
        else
        {
            infoPanel.SetText(Language.GetLanguageDictionary(Language.CurrentLanguage)[LanguageKeys.CHECK_CONNECTION]);
            infoPanel.OpenClosePanel(true);
            Debug.Log("not ready");
        }
    }

    public void ChangeSound(bool on)
    {
        GameManager.Instance.PlayMusic(on);
        soundOn.SetActive(on);
        soundOff.SetActive(!on);
    }

    public void BuyColorPressed()
    {
        if (GameManager.Instance.PurchaseManager.BuyWithCost(colorController.GetColorDataById(currentColorDataId).cost))
        {
            if (!CurrentCarData.colorIds.Contains(currentColorDataId))
            {
                currentCarData.colorIds.Add(currentColorDataId);
                modificationManager.SaveCarData();
            }
        }
        else
        {
            infoPanel.SetText(Language.GetLanguageDictionary(Language.CurrentLanguage)[LanguageKeys.NOT_ENOUGH_MONEY]);
            infoPanel.OpenClosePanel(true);
        }
    }

    public void BuyRimPressed()
    {
        Debug.Log("BuyRimPressed");
        if (GameManager.Instance.PurchaseManager.BuyWithCost(rimController.GetRimDataById(currentRimDataId).cost))
        {
            if (!CurrentCarData.rimIds.Contains(currentRimDataId))
            {
                Debug.Log("currentCarData.rimIds.Add");
                currentCarData.rimIds.Add(currentRimDataId);
                modificationManager.SaveCarData();
            }
        }
        else
        {
            infoPanel.SetText(Language.GetLanguageDictionary(Language.CurrentLanguage)[LanguageKeys.NOT_ENOUGH_MONEY]);
            infoPanel.OpenClosePanel(true);
        }
    }

    public void BuyCarPressed()
    {
        if (GameManager.Instance.PurchaseManager.BuyWithCost(CurrentCarData.cost))
        {
            modificationManager.SaveCarData();
        }
        else
        {
            infoPanel.SetText(Language.GetLanguageDictionary(Language.CurrentLanguage)[LanguageKeys.NOT_ENOUGH_MONEY]);
            infoPanel.OpenClosePanel(true);
        }
    }

    public void ColorPanelPressed()
    {
        UpdateCar();
    }

    public void RimPanelPressed()
    {
        UpdateCar();
    }

    public void GoBack()
    {
        //unregister events
        modificationManager.OnSelectedCarChanged -= ModificarionUIController_OnSelectedCarChanged;
        modificationManager.OnCarSaved -= ModificationManager_OnCarSaved;
        OnSelectedCarChanged = colorController.OnSelectedCarChanged;
        OnSelectedCarChanged = rimController.OnSelectedCarChanged;
        Language.OnLanguageChanged -= Language_OnLanguageChanged;
        GameManager.Instance.DataManager.OnTotalMoneyChanged -= Instance_OnTotalMoneyChanged;

        //change scene
        GameManager.Instance.GoMainScreen();
    }

    public void ShowCurrentCarLeaderboard()
    {
        string id = currentCarData.leaderboardId;
        Debug.Log(id);
        LeaderboardController.Instance.ShowLeaderboardWithId(id);
    }
}
