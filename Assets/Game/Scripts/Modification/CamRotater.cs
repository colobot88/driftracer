﻿using UnityEngine;
using System.Collections;

public class CamRotater : MonoBehaviour {

    public Transform target;//the target object
    public float speedMod = 10.0f;//a speed modifier
    private Vector3 point;//the coord to the point where the camera looks at

    public Transform probe;

    public float f_lastX = 0.0f;
    public float f_difX = 1.0f;
    public float difSpeed = .1f;
    public bool stopped = false;
    int x_direction = 1;

    void Start()
    {

    }

    public void SetTarget(Transform tar)
    {
        this.target = tar;
        point = target.position;//get target's coords
        transform.LookAt(point);//makes the camera look to it
    } 

    void LateUpdate()
    {
        if (Application.platform == RuntimePlatform.WindowsEditor)
        {
            if (Input.GetMouseButton(0))
            {
                stopped = true;
                f_difX = f_lastX - Input.GetAxis("Mouse X");
                difSpeed = Mathf.Abs(f_difX);

                if (f_lastX < Input.GetAxis("Mouse X"))
                {
                    x_direction = -1;
                }

                if (f_lastX > Input.GetAxis("Mouse X"))
                {
                    x_direction = 1;
                }

                //makes the camera rotate around "point" coords, rotating around its Y axis, 15 degrees per second times the speed modifier

                transform.RotateAround(point, new Vector3(0.0f, -f_difX, 0.0f), difSpeed);

                f_lastX = -Input.GetAxis("Mouse X");
            }
            else
            {
                f_difX = Mathf.Abs(f_difX);

                if (stopped)
                {
                    if (difSpeed > 3f) difSpeed = 3.0f;
                    if (difSpeed > .1f) difSpeed -= 0.05f;
                    if (difSpeed < .1f) difSpeed += 0.05f;
                }
                transform.RotateAround(point, new Vector3(0.0f, -f_difX * x_direction, 0.0f), difSpeed);
            }
        }
        else
        {
            if (Input.touchCount > 0/*Input.GetMouseButton(0)*/)
            {
                Debug.Log("touch");
                f_lastX = Input.GetTouch(0).position.x;//-Input.GetAxis("Mouse X");
                stopped = true;
                //f_difX = f_lastX - Input.GetTouch(0).position.x; //Input.GetAxis("Mouse X");
                Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;
                f_difX = -touchDeltaPosition.x/4;
                difSpeed = Mathf.Abs(f_difX);

                if (f_difX < 0/*Input.GetAxis("Mouse X")*/)
                {
                    x_direction = -1;
                }

                if (f_difX > 0 /*Input.GetAxis("Mouse X")*/)
                {
                    x_direction = 1;
                }

                //makes the camera rotate around "point" coords, rotating around its Y axis, 15 degrees per second times the speed modifier

                transform.RotateAround(point, new Vector3(0.0f, -f_difX, 0.0f), difSpeed);

                f_lastX = Input.GetTouch(0).position.x /*-Input.GetAxis("Mouse X")*/;
            }
            else
            {
                f_difX = Mathf.Abs(f_difX);

                if (stopped)
                {
                    if (difSpeed > 3f) difSpeed = 3.0f;
                    if (difSpeed > .1f) difSpeed -= 0.05f;
                    if (difSpeed < .1f) difSpeed += 0.05f;
                }
                transform.RotateAround(point, new Vector3(0.0f, -f_difX * x_direction, 0.0f), difSpeed);
            }
        }
    }
}
