﻿using UnityEngine;
using System.Collections;

public class SpinLogic : MonoBehaviour
{
    bool stopped = false;
    float f_lastX = 0.0f;
    public float f_difX = 0.0f;
    float f_lastY = 0.0f;
    public float f_difY = 0.0f;
    float f_steps = 0.0f;
    int x_direction = 1;
    int y_direction = 1;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            stopped = true;
            f_difX = 0.0f;
            f_difY = 0.0f;
        }
        else if (Input.GetMouseButton(0))
        {
            f_difX = Mathf.Abs(f_lastX - Input.GetAxis("Mouse X"));
            f_difY = Mathf.Abs(f_lastY - Input.GetAxis("Mouse Y"));

            if (f_lastX < Input.GetAxis("Mouse X"))
            {
                x_direction = -1;
                transform.Rotate(Vector3.up, -f_difX);
            }

            if (f_lastX > Input.GetAxis("Mouse X"))
            {
                x_direction = 1;
                transform.Rotate(Vector3.up, f_difX);
            }

            if (f_lastY < Input.GetAxis("Mouse Y"))
            {
                y_direction = -1;
                //transform.Rotate(Vector3.left, f_difY);
            }

            if (f_lastY > Input.GetAxis("Mouse Y"))
            {
                y_direction = 1;
                //transform.Rotate(Vector3.left, -f_difY);
            }

            f_lastX = -Input.GetAxis("Mouse X");
            f_lastY = -Input.GetAxis("Mouse Y");
        }
        else 
        {
            if (f_difX > 3f) f_difX = 3.0f;
            if (f_difX > 0.0f) f_difX -= 0.05f;
            if (f_difX < 0.0f) f_difX += 0.05f;

            if (f_difY > 3f) f_difY = 3.0f;
            if (f_difY > 0.0f) f_difY -= 0.05f;
            if (f_difY < 0.0f) f_difY += 0.05f;

            transform.Rotate(Vector3.up, (f_difX) * x_direction);
            //transform.Rotate(Vector3.left, (f_difY) * -y_direction);
        }
    }
}