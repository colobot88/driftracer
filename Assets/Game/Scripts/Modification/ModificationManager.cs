﻿using UnityEngine;
using UnityStandardAssets.Vehicles.Car;

public class ModificationManager : MonoBehaviour {

    //Cars list
    public AvailableCars avCars; //all cars in ordered list, changes in list does not affect the game (if player has car with id = 2 and if the car's order in list changed there is no problem.)
    //Current carData
    public CarDataMono currentCarData; //current car data (color, rim id)
    //events
    public delegate void OnSelectedCarChangedEvent(CarController carController);
    public event OnSelectedCarChangedEvent OnSelectedCarChanged;

    public delegate void OnCarSavedEvent();
    public event OnCarSavedEvent OnCarSaved;

    public CameraMove cameraMove;

    void Awake()
    {
        Time.timeScale = 1;
        LoadCars();
    }

    private void LoadCars() //load cars from the list
    {
        for (int i = 0; i < avCars.cars.Count; i++)
        {
            GameObject car = avCars.cars[i].gameObject;
            car.transform.position = new Vector3(0f, .3f, 0.5f);
            car.SetActive(false);
        }
        SetCurrentCarByIndex(GetLastSelectedCarIndex());//gets last selected car id from dataManager and set it as selected car (setactive(true))
    }

    public CarDataMono GetCurrentCarDataMono()
    {
        return currentCarData;
    }

    // only on Start
    public int GetLastSelectedCarIndex()
    {
        string uuid = GameManager.Instance.DataManager.PlayerData.CurrentCarUuid;
        for (int i = 0; i < avCars.cars.Count; i++)
            if (avCars.cars[i].uuid == uuid)
                return i;
        return 0;
    }

    // only on Start in ModificationUIController
    public int GetTotalCarCount()
    {
        return this.avCars.cars.Count;
    }

    //changes selected car
    public CarDataMono SetCurrentCarByIndex(int index)
    {
        if (currentCarData != null)
        {
            currentCarData.transform.parent = null;
            currentCarData.gameObject.SetActive(false);
        }

        currentCarData = avCars.cars[index];
        currentCarData.gameObject.SetActive(true);


        MonoBehaviour[] comps = currentCarData.gameObject.GetComponents<MonoBehaviour>();
        foreach (MonoBehaviour c in comps)
        {
            bool flag = c is CarDataMono;
            c.enabled = flag;
        }

        currentCarData.SetCarData(GameManager.Instance.DataManager.PlayerData.GetCarDataById(currentCarData.uuid));

        if (OnSelectedCarChanged != null)
            OnSelectedCarChanged(currentCarData.gameObject.GetComponent<CarController>());
        GameManager.Instance.SetCurrentCarObj(currentCarData.gameObject);

        cameraMove.target = currentCarData.transform;

        return currentCarData;
    } 
    
    //get carcontroller of selected car to display acceleration and maxspeed value
    public CarController GetCurrentCarController()
    {
        return this.currentCarData.GetComponent<CarController>();
    }

    public void SaveCarData()
    { 
        CarData cdata = currentCarData.GetCarData();
        GameManager.Instance.DataManager.SaveCarData(cdata);
        GameManager.Instance.DataManager.PlayerData.SetCarData(cdata);
        GameManager.Instance.DataManager.PlayerData.CurrentCarId = currentCarData.Id;
        GameManager.Instance.DataManager.PlayerData.CurrentCarUuid = currentCarData.uuid;
        GameManager.Instance.DataManager.SavePlayerData();
        if (OnCarSaved != null)
            OnCarSaved();
    }

    #region Modify Car
    public void ChangeCarColor(Color color, int id)
    {
        if(currentCarData != null)
        {
            currentCarData.currentColorId = id;
            currentCarData.Color = color;
        }
    }

    public void ChangeCarRim(int id)
    {
        currentCarData.RimId = id;
    }
    #endregion
}
