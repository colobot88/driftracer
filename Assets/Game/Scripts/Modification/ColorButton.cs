﻿using UnityEngine;

public class ColorButton : MonoBehaviour
{
    public int id;
    public Color color;
    public int cost;
    public GameObject lockImage;
}