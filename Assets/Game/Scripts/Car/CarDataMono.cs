﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class CarDataMono : MonoBehaviour
{
    public int id;
    public string uuid;
    public string leaderboardId;
    public int cost;
    public Color color;
    public int currentColorId;
    public List<int> colorIds;
    public List<int> rimIds;
    public int rimId;
    public new Renderer renderer;
    public int order;

    public GameObject wheelFR;
    public GameObject wheelFL;
    public GameObject wheelRR;
    public GameObject wheelRL;


    public void Start()
    {
        //color = renderer.material.color;
    }

    public int Id
    {
        get
        {
            return id;
        }
        set
        {
            id = value;
        }
    }

    public int RimId
    {
        get
        {
            return rimId;
        }
        set
        {
            if(rimId != value)
            {
                rimId = value;
                GameObject newWheelFR = Instantiate(Resources.Load("cars/rims/" + rimId + "/Wheel_R", typeof(GameObject))) as GameObject;
                GameObject newWheelRR = Instantiate(Resources.Load("cars/rims/" + rimId + "/Wheel_R", typeof(GameObject))) as GameObject;
                GameObject newWheelFL = Instantiate(Resources.Load("cars/rims/" + rimId + "/Wheel_L", typeof(GameObject))) as GameObject;
                GameObject newWheelRL = Instantiate(Resources.Load("cars/rims/" + rimId + "/Wheel_L", typeof(GameObject))) as GameObject;

                newWheelFR.transform.parent = wheelFR.transform.parent;
                newWheelFR.transform.position = wheelFR.transform.position;
                newWheelFR.transform.rotation = wheelFR.transform.rotation;
                Destroy(wheelFR);
                wheelFR = newWheelFR;

                newWheelRR.transform.parent = wheelRR.transform.parent;
                newWheelRR.transform.position = wheelRR.transform.position;
                newWheelRR.transform.rotation = wheelRR.transform.rotation;
                Destroy(wheelRR);
                wheelRR = newWheelRR;

                newWheelFL.transform.parent = wheelFL.transform.parent;
                newWheelFL.transform.position = wheelFL.transform.position;
                newWheelFL.transform.rotation = wheelFL.transform.rotation;
                Destroy(wheelFL);
                wheelFL = newWheelFL;

                newWheelRL.transform.parent = wheelRL.transform.parent;
                newWheelRL.transform.position = wheelRL.transform.position;
                newWheelRL.transform.rotation = wheelRL.transform.rotation;
                Destroy(wheelRL);
                wheelRL = newWheelRL;
            }
        }
    }

    public Color Color
    {
        get
        {
            return color;
        }
        set
        {
            color = value;
            renderer.material.color = color;
            renderer.material.SetColor("_ReflectColor", color);
        }
    }

    public CarData GetCarData()
    {
        CarData carData = new CarData(uuid);

        carData.currentColorId = this.currentColorId;
        carData.ColorR = Color.r;
        carData.ColorG = Color.g;
        carData.ColorB = Color.b;
        carData.ColorA = Color.a;

        carData.RimId = RimId;

        carData.colorIds = this.colorIds;
        carData.rimIds = this.rimIds;

        carData.cost = this.cost;

        return carData;
    }

    public void SetCarData(CarData carData)
    {
        Color color = new Color(carData.ColorR, carData.ColorG, carData.ColorB, carData.ColorA);
        this.currentColorId = carData.currentColorId;
        this.Color = color;
        this.RimId = carData.RimId;
        this.colorIds = carData.colorIds;
        this.rimIds = carData.rimIds;
    }
}
