﻿using UnityEngine;
using System.Collections;

public class AntiSpin : MonoBehaviour {

    public Transform target;
    public float antiSpinAmount = 3f;
    private Rigidbody rb;

    private ParticleSystem.EmissionModule emission;
    public WheelCollider[] wheels;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
        bool targetOnLeft = transform.InverseTransformPoint(target.position).x < -2;
        bool targetOnRight = transform.InverseTransformPoint(target.position).x > 2;
        bool rotatingToLeft = rb.angularVelocity.y < 0;


        if ((targetOnLeft && !rotatingToLeft) || (targetOnRight && rotatingToLeft))
            rb.angularDrag = antiSpinAmount;
        else
            rb.angularDrag = 0;
    }
}
