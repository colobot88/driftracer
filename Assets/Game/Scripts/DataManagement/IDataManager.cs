﻿using UnityEngine;
using System.Collections;

public delegate void OnTotalMoneyChangedEvent(int totalMoney);

public interface IDataManager{

    void Initialize();
    void SavePlayerData();
    byte[] GetPlayerDataAsByteArray();
    void LoadPlayerData();
    void SaveCarData(CarData carData);
    CarData LoadCarData(string id);

    PlayerData PlayerData
    {
        get;
    }

    void AdsManager_OnMoneyAdWatched(int reward);
    void PurchaseItem(int cost);
    void EarnMoney(int amount);
    event OnTotalMoneyChangedEvent OnTotalMoneyChanged;
}
