﻿using System;

[Serializable]
public class DrScore
{
    public string lbId;
    public int score;
    public bool sent = false;
}