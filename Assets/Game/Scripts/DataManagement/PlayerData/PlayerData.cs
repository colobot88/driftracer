﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Runtime.Serialization;

[Serializable]
public class PlayerData {

    public int currentLanguage;
    public int currentCarId = 0; //players current car
    public string currentCarUuid;
    public int totalMoney = 0;
    public int highestScore = 0;
    public int winnerScore = 0;
    public bool rated = false;
    public List<DrScore> scores = new List<DrScore>();
    public List<string> carIds = new List<string>() { "9f825284-3969-4697-9dd0-59c97c88086e" }; //cars which player has

    [IgnoreDataMember]
    private Dictionary<string, CarData> carDataDict = new Dictionary<string, CarData>();

    public PlayerData(int _currentCarId = 0, List<string> _carIds = null, int lang = 0)
    {
        this.currentCarId = _currentCarId;
        this.CurrentLanguage = lang;
        if(_carIds != null)
            this.carIds = _carIds;
    }
    
    public int CurrentCarId
    {
        get
        {
            return currentCarId;
        }
        set
        {
            this.currentCarId = value;
        }
    }

    public List<string> CarIds
    {
        get
        {
            return carIds;
        }
        set
        {
            this.carIds = value;
        }
    }

    public Dictionary<string, CarData> CarDataDict
    {
        get
        {
            return carDataDict;
        }
        set
        {
            carDataDict = value;
        }
    }

    public int TotalMoney
    {
        get
        {
            return totalMoney;
        }

        set
        {
            totalMoney = value;
        }
    }

    public int CurrentLanguage
    {
        get
        {
            return currentLanguage;
        }

        set
        {
            currentLanguage = value;
        }
    }

    public int HighestScore
    {
        get
        {
            return highestScore;
        }

        set
        {
            if (value > highestScore)
                highestScore = value;
        }
    }

    public string CurrentCarUuid
    {
        get
        {
            return currentCarUuid;
        }

        set
        {
            currentCarUuid = value;
        }
    }

    internal List<DrScore> Scores
    {
        get
        {
            return scores;
        }

        set
        {
            scores = value;
        }
    }

    public bool Rated
    {
        get
        {
            return rated;
        }

        set
        {
            rated = value;
        }
    }

    public bool hasCarWithId(string id)
    {
        return carIds.Contains(id);
    }

    public void AddCarWithId(string id)
    {
        Debug.Log(id);
        carIds.Add(id);
    }

    public void AddCarData(CarData carData)
    {
        if(!this.carDataDict.ContainsKey(carData.Uuid))
            this.CarDataDict.Add(carData.Uuid, carData);
    }

    public CarData GetCarDataById(string uuid)
    {
        CarData cData;
        CarDataDict.TryGetValue(uuid, out cData);
        if (cData == null)
            cData = new CarData(uuid);
        return cData;
    }

    public void SetScoreWithLbId(string lbId, int score, bool sent = false)
    {
        DrScore drScore = GetDrScoreWithLbId(lbId);
        if(drScore != null)
        {
            if (score > drScore.score)
                drScore.score = score;
        }
        else
        {
            drScore = new DrScore();
            drScore.lbId = lbId;
            drScore.score = score;
            Scores.Add(drScore);
        }
        drScore.sent = sent;

    }

    private DrScore GetDrScoreWithLbId(string lbId)
    {
        if (Scores == null)
            Scores = new List<DrScore>();
        foreach(DrScore score in Scores)
        {
            if (score.lbId == lbId)
                return score;
        }
        return null;
    }

    public CarData SetCarData(CarData data)
    {
        if (CarDataDict.ContainsKey(data.Uuid))
            CarDataDict[data.Uuid] = data;
        else
        {
            CarDataDict.Add(data.Uuid, data);
            this.AddCarWithId(data.Uuid);
        }
        return data;
    }
}
