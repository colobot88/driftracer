﻿using UnityEngine;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class DataManager : IDataManager {

    static readonly DataManager _instance = new DataManager();
    public static DataManager Instance
    {
        get
        {
            return _instance;
        }
    }
    DataManager()
    {

    }

    private PlayerData playerData;

    public event OnTotalMoneyChangedEvent OnTotalMoneyChanged;

    public void Initialize()
    {
        playerData = new PlayerData();
        LoadPlayerData();
    }

    public PlayerData PlayerData
    {
        get
        {
            return this.playerData;
        }
    }

    public byte[] GetPlayerDataAsByteArray()
    {
        BinaryFormatter bf = new BinaryFormatter();
        MemoryStream ms = new MemoryStream();
        bf.Serialize(ms, PlayerData);
        byte[] myByteArray = ms.ToArray();
        return myByteArray;
    }

    public void SavePlayerData()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(Application.persistentDataPath + "/playerData.dat", FileMode.Create);
        bf.Serialize(file, PlayerData);
        file.Close();
    }

    public void LoadPlayerData()
    {
        Debug.Log(Application.persistentDataPath);
        if (File.Exists(Application.persistentDataPath + "/playerData.dat"))
        {
            try
            {
                BinaryFormatter bf = new BinaryFormatter();
                FileStream file = File.Open(Application.persistentDataPath + "/playerData.dat", FileMode.Open);
                this.playerData = (PlayerData)bf.Deserialize(file);
                playerData.CarDataDict.Clear();
                file.Close();

                foreach (string id in playerData.CarIds)
                {
                    CarData cData = LoadCarData(id);
                    if (cData != null)
                        playerData.AddCarData(cData);
                }
            } catch (Exception e)
            {
                Debug.Log(e.ToString());
            }
        }
        else
        {
            //SavePlayerData();
            //CarData carData = new CarData("9f825284-3969-4697-9dd0-59c97c88086e");
            //carData.ColorR = 0.639f;
            //carData.ColorG = 0.467f;
            //carData.ColorB = 0.031f;
            //carData.ColorA = 1f;
            //SaveCarData(carData);
        }
    }

    public void SaveCarData(CarData carData)
    {
        Console.WriteLine(carData.ColorR);
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(Application.persistentDataPath + "/" + carData.Uuid + ".dat", FileMode.Create);
        bf.Serialize(file, carData);
        file.Close();
    }

    public CarData LoadCarData(string id)
    {
        if (File.Exists(Application.persistentDataPath + "/" + id + ".dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/" + id + ".dat", FileMode.Open);

            CarData carData = (CarData)bf.Deserialize(file);
            file.Close();

            return carData;
        }

        return null;
    }

    public void AdsManager_OnMoneyAdWatched(int reward)
    {
        PlayerData.TotalMoney += reward;
        if (OnTotalMoneyChanged != null)
            OnTotalMoneyChanged(PlayerData.TotalMoney);
        SavePlayerData();
    }

    public void EarnMoney(int amount)
    {
        PlayerData.TotalMoney += amount;
        if (OnTotalMoneyChanged != null)
            OnTotalMoneyChanged(PlayerData.TotalMoney);
        SavePlayerData();
    }

    public void PurchaseItem(int cost)
    {
        PlayerData.TotalMoney -= cost;
        if (OnTotalMoneyChanged != null)
            OnTotalMoneyChanged(PlayerData.TotalMoney);
        SavePlayerData();
    }
}
