﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class CarData{

    public int id;
    public string uuid;
    public int cost;
    public int rimId;
    public List<int> colorIds;
    public List<int> rimIds;
    public int currentColorId;
    public float colorR;
    public float colorG;
    public float colorB;
    public float colorA;

    public CarData(string uuid)
    {
        //this.Id = id;
        this.Uuid = uuid;
        currentColorId = 0;
        ColorR = 0.639f;
        ColorG = 0.467f;
        ColorB = 0.031f;
        ColorA = 1f;
        cost = 0;
        colorIds = new List<int> { 0, 1, 2, 3 };
        rimIds = new List<int> { 0, 1, 2, 3 };
    }

    public int Id
    {
        get
        {
            return id;
        }
        set
        {
            id = value;
        }
    }

    public int RimId
    {
        get
        {
            return rimId;
        }
        set
        {
            rimId = value;
        }
    }

    public float ColorR
    {
        get
        {
            return colorR;
        }
        set
        {
            colorR = value;
        }
    }
    public float ColorG
    {
        get
        {
            return colorG;
        }
        set
        {
            colorG = value;
        }
    }
    public float ColorB
    {
        get
        {
            return colorB;
        }
        set
        {
            colorB = value;
        }
    }
    public float ColorA
    {
        get
        {
            return colorA;
        }
        set
        {
            colorA = value;
        }
    }

    public string Uuid
    {
        get
        {
            return uuid;
        }

        set
        {
            uuid = value;
        }
    }
}
