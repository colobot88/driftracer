﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Advertisements;
using System;
using UnityEngine.Analytics;
using System.Collections.Generic;

public class UIController : MonoBehaviour
{
    public Text liveScore;
    //panels
    public GameObject gamePanel;
    public GameObject continuePanel;
    public GameObject gameOverPanel;
    public InfoPanel infoPanel;

    //Button Texts
    public Text continueButtonText;
    public Text continueMoneyButtonText;
    public Text continueVideoButtonText;
    public Text restartButtonText;
    public Text restartGOButtonText;
    public Text homeButtonText;
    public Text homeGOButtonText;
    public Text gameOverPanelText;

    public Text continueText;
    public Text noText;

    public Text playerMoneyText;

    //speedIndicator
    public Image speedIndicatorImage;
    public Text speedIndicator;

    public Image sliderInner;

    public Text gamestateText;

    public Slider slider;
    public Text timeLeft;
    private float leftTime;
    public Text totalDistance;
    public float totalDistanceFloat;
    public Text totalDriftTime;
    public int highScore;
    public Text scoreText;
    public Text score;
    public int totalMoneyInt;
    public Text totalMoney;
    public Text distanceText;
    public Text driftTimeText;
    public Text leftTimeGameOver;
    public Text leftTimeGameOverInfoText;
    public Image leftTimeGameOverIcon;
    public Image leftTimeGameOverImg;

    public Text qualityText;

    public Text playbackLeftCount;
    public Text playbackMoneyLeftCount;
    public Text videoplaybackLeftCount;

    public Text videoText;
    public Text playbackMoneyAmountText;

    public GameObject continueButton;
    public GameObject videoButton;
    public GameObject playbackMoneyButton;

    public Transform timePoint;

    public Image nitrousImage;

    // Event Handler
    public delegate void OnSliderChangedEvent(float g);
    public event OnSliderChangedEvent OnSliderChanged;

    public delegate void OnNitrousClickedEvent(bool active);
    public event OnNitrousClickedEvent OnNitrousClicked;

    // Use this for initialization
    void Start()
    {
        if(GameManager.Instance != null)
        {
            GameManager.Instance.adsManager.OnAdWatched += AdsManager_OnAdWatched;
            Language.OnLanguageChanged += Language_OnLanguageChanged;
            Language_OnLanguageChanged(Language.CurrentLanguage);
        }
        GamePlayManager.Instance.TimeManager.OnTimeLeftChanged += TimeManager_OnTimeLeftChanged;
        GamePlayManager.Instance.CarManager.OnTimePoint += CarManager_OnTimePoint;
        PlaybackManager.Instance.OnLeftCountChanged += Instance_OnLeftCountChanged;
        GamePlayManager.Instance.GameState.OnGameStateChanged += GameState_OnGameStateChanged;
        GamePlayManager.Instance.CarManager.OnScoreChanged += CarManager_OnScoreChanged;
        Settings.OnQualityChanged += Settings_OnQualityChanged;
    }

    private void CarManager_OnScoreChanged(float score)
    {
        highScore = (int)score;
        liveScore.text = "SCORE: " + score.ToString("N0");
    }

    private void Language_OnLanguageChanged(Languages current)
    {
        Debug.Log("Language_OnLanguageChanged " + Language.CurrentLanguage);
        Debug.Log(Language.GetLanguageDictionary(current)[LanguageKeys.CONTINUE]);
        continueButtonText.text = Language.GetLanguageDictionary(current)[LanguageKeys.CONTINUE];
        continueText.text = Language.GetLanguageDictionary(current)[LanguageKeys.CONTINUE] + " ?";
        noText.text = Language.GetLanguageDictionary(current)[LanguageKeys.NO];
        continueMoneyButtonText.text = Language.GetLanguageDictionary(current)[LanguageKeys.CONTINUE];
        continueVideoButtonText.text = Language.GetLanguageDictionary(current)[LanguageKeys.CONTINUE];
        restartButtonText.text = Language.GetLanguageDictionary(current)[LanguageKeys.RESTART];
        restartGOButtonText.text = Language.GetLanguageDictionary(current)[LanguageKeys.RESTART];
        homeButtonText.text = Language.GetLanguageDictionary(current)[LanguageKeys.GO_HOME];
        homeGOButtonText.text = Language.GetLanguageDictionary(current)[LanguageKeys.GO_HOME];
        gameOverPanelText.text = Language.GetLanguageDictionary(current)[LanguageKeys.GAME_OVER];
        driftTimeText.text = Language.GetLanguageDictionary(current)[LanguageKeys.DRIFT_TIME];
        leftTimeGameOverInfoText.text = Language.GetLanguageDictionary(current)[LanguageKeys.LEFT_TIME];
        distanceText.text = Language.GetLanguageDictionary(current)[LanguageKeys.DISTANCE];
        scoreText.text = Language.GetLanguageDictionary(current)[LanguageKeys.SCORE];
        videoText.text = Language.GetLanguageDictionary(current)[LanguageKeys.VIDEO];
    }

    private void Settings_OnQualityChanged(GraphicsQuality quality)
    {
        qualityText.text = quality.ToString();
    }

    private void CarManager_OnTimePoint(Vector3 carPosition, float time)
    {
        CreateTimePoint(carPosition, time);
    }

    private void GameState_OnGameStateChanged(GAMESTATES gs)
    {
        totalDriftTime.text = GamePlayManager.Instance.CarManager.TotalDriftTime.ToString("N0");
        totalMoneyInt = (int)(totalDistanceFloat + GamePlayManager.Instance.CarManager.TotalDriftTime);
        totalMoney.text = totalMoneyInt.ToString("N0");
        //highScore = (int)(totalDistanceFloat * GamePlayManager.Instance.CarManager.TotalDriftTime / 10);
        //highScore = (int)liveScore;
        score.text = highScore.ToString();

        this.leftTimeGameOver.text = leftTime.ToString("N2");
        gameOverPanelText.text = Language.GetLanguageDictionary(Language.CurrentLanguage)[LanguageKeys.GAME_OVER];
        playerMoneyText.text = GameManager.Instance.DataManager.PlayerData.TotalMoney.ToString();

        if (gs == GAMESTATES.FINISH_GAMEOVER)
            FinishGameOver();

        if (gs == GAMESTATES.TIME_OVER || gs == GAMESTATES.FINISH_GAMEOVER)
            GameOver(false);
        else if(gs == GAMESTATES.OFF_ROAD || gs == GAMESTATES.WRONG_WAY)
            GameOver(true);
    } 

    private void FinishGameOver()
    {
        highScore = (int)(highScore*leftTime);
        score.text = highScore.ToString();
        this.score.text = (totalDistanceFloat * (1 + leftTime)).ToString();
        leftTimeGameOver.color = new Color(leftTimeGameOver.color.r, leftTimeGameOver.color.g, leftTimeGameOver.color.b, 1);
        leftTimeGameOverIcon.color = new Color(leftTimeGameOver.color.r, leftTimeGameOver.color.g, leftTimeGameOver.color.b, 1);
        leftTimeGameOverImg.color = new Color(leftTimeGameOver.color.r, leftTimeGameOver.color.g, leftTimeGameOver.color.b, 1);
        leftTimeGameOverInfoText.color = new Color(leftTimeGameOver.color.r, leftTimeGameOver.color.g, leftTimeGameOver.color.b, 1);
        gameOverPanelText.text = Language.GetLanguageDictionary(Language.CurrentLanguage)[LanguageKeys.GAME_WIN];
        gameOverPanelText.color = Color.green;
    }

    private void TimeManager_OnTimeLeftChanged(float timeLeft)
    {
        if (timeLeft < 0)
            timeLeft = 0f;
        SetTimeLeft(timeLeft);
    }

    private void Instance_OnLeftCountChanged(int leftCount)
    {
        playbackLeftCount.text = leftCount.ToString();
        playbackMoneyLeftCount.text = leftCount.ToString();
        videoplaybackLeftCount.text = playbackLeftCount.text;
    }

    private void AdsManager_OnAdWatched(bool success)
    {
        Debug.Log("cancontinue " + success);
        if (PlaybackManager.Instance.LeftCount > 0)
        {
            SetActiveContinueButton(success);
            videoButton.SetActive(!success);
            playbackMoneyButton.SetActive(!success);
            if(!success)
            {
                infoPanel.SetText(Language.GetLanguageDictionary(Language.CurrentLanguage)[LanguageKeys.WATCH_TO_END]);
                infoPanel.OpenClosePanel(true);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        UpdateCarSpeedIndicator();
        SetTotalDistance(GamePlayManager.Instance.CarManager.TotalDistance);
        if (Input.GetKeyDown(KeyCode.Space))
            UseNitrous(true);
        if (Input.GetKeyUp(KeyCode.Space))
            UseNitrous(false);
    }

    private void UpdateCarSpeedIndicator()
    {
        int currentSpeed = (int)(GamePlayManager.Instance.CarManager.CarController.GetCurrentSpeed() * 1.5f);
        speedIndicator.text = currentSpeed.ToString();
        speedIndicatorImage.fillAmount = GamePlayManager.Instance.CarManager.CarController.GetCurrentSpeedRatio();
    }

    public void GameOver(bool canContinue = true)
    {
        continuePanel.SetActive(canContinue);
        gamePanel.SetActive(false);

        if (!canContinue)
        {
            SendHighScore();
            gameOverPanel.SetActive(true);

            GameManager.Instance.DataManager.EarnMoney(totalMoneyInt);
            continueButton.SetActive(false);
            videoButton.SetActive(false);
            playbackMoneyButton.SetActive(false);
        }
        else
        {
            if (!PlaybackManager.Instance.PlaybackReady)
            {
                continuePanel.SetActive(false);
                gameOverPanel.SetActive(true);
                SendHighScore();

                GameManager.Instance.DataManager.EarnMoney(totalMoneyInt);
                continueButton.SetActive(false);
                videoButton.SetActive(false);
                playbackMoneyButton.SetActive(false);
                return;
            }
            if (PlaybackManager.Instance.IsOldPlayback())
            {
                if(PlaybackManager.Instance.PlaybackReady)
                    continueButton.SetActive(true);
                videoButton.SetActive(false);
                playbackMoneyButton.SetActive(false);
            }
            else if (PlaybackManager.Instance.LeftCount > 0)
            {
                int random = UnityEngine.Random.Range(0,100);
                float totalDistance = GamePlayManager.Instance.CarManager.TotalDistance;
                bool showAd = false;
                if (totalDistance < 500)
                {
                    showAd = false;
                }
                else if (totalDistance < 1500)
                {
                    if (random > 80)
                        showAd = true;
                    else
                        showAd = false;
                }
                else if (totalDistance < 3000)
                {
                    if (random > 70)
                        showAd = true;
                    else
                        showAd = false;
                }
                else if (totalDistance < 4500)
                {
                    if (random > 60)
                        showAd = true;
                    else
                        showAd = false;
                }
                else if (totalDistance < 6000)
                {
                    if (random > 40)
                        showAd = true;
                    else
                        showAd = false;
                }
                else if (totalDistance < 8000)
                {
                    if (random > 20)
                        showAd = true;
                    else
                        showAd = false;
                }
                else if (totalDistance >= 8000)
                    showAd = true;
                continueButton.SetActive(!showAd);
                videoButton.SetActive(showAd);
                playbackMoneyAmountText.text = (this.totalMoneyInt * 3).ToString();
                playbackMoneyButton.SetActive(showAd);
            }
            else
            {
                gameOverPanel.SetActive(true);
                SendHighScore();
                GameManager.Instance.DataManager.EarnMoney(totalMoneyInt);
                Debug.Log("money " + totalMoneyInt);
                continuePanel.SetActive(false);
                gamePanel.SetActive(false);
            }
        }
    }

    public void SendHighScore()
    {
        int random = UnityEngine.Random.Range(0, 100);
        if (this.highScore > 5000 && (this.highScore > GameManager.Instance.DataManager.PlayerData.HighestScore || random > 90))
        {
            if (!GameManager.Instance.DataManager.PlayerData.Rated)
                GameManager.Instance.askRate = true;
        }
        GameManager.Instance.DataManager.PlayerData.HighestScore = highScore;
        LeaderboardController.Instance.AddHighScoreToLeaderBoard(GameManager.Instance.DataManager.PlayerData.HighestScore);

        string lbId = GamePlayManager.Instance.CarManager.GetCarDataMono().leaderboardId;
        GameManager.Instance.DataManager.PlayerData.SetScoreWithLbId(lbId, highScore, false);
        GameManager.Instance.DataManager.SavePlayerData();
        LeaderboardController.Instance.AddHighScoreToLeaderBoardWithId(lbId, highScore);
    }

    public void OnSliderValueChanged(Slider slider)
    {
        sliderInner.fillAmount = (slider.value + 1) / 2;
        if (GamePlayManager.Instance.GameState.CurrentGameState != GAMESTATES.PLAYBACK)
        {
            GamePlayManager.Instance.GameState.CurrentGameState = GAMESTATES.PLAYING;
        }
        GamePlayManager.Instance.CarManager.carControl.Accelinput = slider.value;
    }

    public void Restart()
    {
        this.SendRestartAnalytics();
        continuePanel.SetActive(false);
        gamePanel.SetActive(true);
        gameOverPanel.SetActive(false);
        slider.value = 0f;
        GamePlayManager.Instance.OnRestartClicked();
        if(totalDistanceFloat < 800)
        {
            infoPanel.SetText(Language.GetLanguageDictionary(Language.CurrentLanguage)[LanguageKeys.TUTORIAL_STEER]);
            infoPanel.OpenClosePanel(true);
        }
    }

    private void SendRestartAnalytics()
    {
        int score = this.highScore;
        string carName = GamePlayManager.Instance.CarManager.GetCarDataMono().gameObject.name;
        Debug.Log(score + " " + carName);
        Analytics.CustomEvent("restart", new Dictionary<string, object>
          {
            { "score", score},
            { "carName", carName}
          });
    }

    public void PlaybackMoney()
    {
        if (GameManager.Instance.PurchaseManager.BuyWithCost(this.totalMoneyInt * 3))
        {
            continueButton.SetActive(false);
            continuePanel.SetActive(false);
            gameOverPanel.SetActive(false);
            gamePanel.SetActive(true);
            slider.value = 0f;
            GamePlayManager.Instance.OnPlaybackClicked();
        }
    }
    public void PlayBack()
    {
        continueButton.SetActive(false);
        continuePanel.SetActive(false);
        gamePanel.SetActive(true);
        gameOverPanel.SetActive(false);
        slider.value = 0f;
        GamePlayManager.Instance.OnPlaybackClicked();
    }

    public void GoHome()
    {

        if (GameManager.Instance != null)
        {
            GameManager.Instance.RestartMusic();
            GameManager.Instance.adsManager.OnAdWatched -= AdsManager_OnAdWatched;
            Language.OnLanguageChanged -= Language_OnLanguageChanged;
            GameManager.Instance.LoadScene(1);
        }
    }

    public void UseNitrous(bool active)
    {
        if (OnNitrousClicked != null)
            OnNitrousClicked(active);
    }

    public void UpdateNitrous(float leftAmount)
    {
        nitrousImage.fillAmount = leftAmount;
    }

    public void ParticleTogglePressed(Text text)
    {
        if (GameManager.Instance != null)
        {
            Settings.ParticlesEnabled = !Settings.ParticlesEnabled;
            text.text = Settings.ParticlesEnabled.ToString();
        }
    }

    public void CreateTimePoint(Vector3 position, float time)
    {
        Transform tpTR = Instantiate(timePoint, timePoint.parent) as Transform;

        TimePoint tp = tpTR.GetComponent<TimePoint>();
        tp.Initialize(position, time.ToString("N1"));

    }

    public void SetTotalDistance(float distance)
    {
        this.totalDistanceFloat = distance;
        this.totalDistance.text = ((int)totalDistanceFloat).ToString();
        //this.totalMoney.text = totalDistanceInt.ToString();
    }

    private void SetTimeLeft(float _lefttime)
    {
        this.leftTime = _lefttime;
        this.timeLeft.text = _lefttime.ToString("N1");
        if (leftTime < 10)
            timeLeft.color = Color.red;
        else
            timeLeft.color = Color.black;
    }

    public void SetActiveContinueButton(bool active)
    {
        if (PlaybackManager.Instance.LeftCount > 0)
        {
            Debug.Log("cont");
            Debug.Log(continueButton);
            continueButton.SetActive(active);
        }
    }

    public void WatchAd()
    {
        if(GameManager.Instance != null)
        {
            if (Advertisement.IsReady())
            {
                GameManager.Instance.adsManager.DisplayAwardedAd();
            }
            else
            {
                infoPanel.SetText(Language.GetLanguageDictionary(Language.CurrentLanguage)[LanguageKeys.CHECK_CONNECTION]);
                infoPanel.OpenClosePanel(true);
                //TODO: show panel displays information
                Debug.Log("not ready");
            }
        }
    }

    internal void playbackLeftCountChanged(int leftCount)
    {
        playbackLeftCount.text = leftCount.ToString();
        playbackMoneyLeftCount.text = leftCount.ToString();
        videoplaybackLeftCount.text = playbackLeftCount.text;
    }
    
    public void ChangeGraphics()
    {
        Settings.ChangeGraphicsQuality();
    }

    public void CancelContinue()
    {
        //GamePlayManager.Instance.GameState.CurrentGameState = GAMESTATES.WRONG_WAY;
        continuePanel.SetActive(false);
        GameOver(false);
    }
}