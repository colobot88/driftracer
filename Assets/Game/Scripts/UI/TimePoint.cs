﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TimePoint : MonoBehaviour {

    public Transform target;
    public Text text;
    public Image image;
    private Vector3 initialPosition;
    private float lifeTime = 0f;
    public bool move = false;

	// Use this for initialization
	void Start () {
        initialPosition = transform.position;
	}

    public void Initialize(Vector3 inPos, string text)
    {
        //this.initialPosition = inPos;
        this.image.gameObject.SetActive(true);
        this.text.text = text;
        move = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (move)
        {
            lifeTime += ((Time.deltaTime) + lifeTime / 2) / 8;
            transform.position = GetCubicCurvePoint(initialPosition, (initialPosition - new Vector3(0f, 300f, 0f)), (target.position - new Vector3(0f, 300f, 0f) / 2), target.position, lifeTime);

            if (Vector3.Distance(transform.position, target.position) < .01f)
                Destroy(gameObject);
        }
    }

    private Vector3 GetCubicCurvePoint(Vector3 p1, Vector3 p2, Vector3 p3, Vector3 p4, float t)
    {
        t = Mathf.Clamp01(t);

        Vector3 part1 = Mathf.Pow(1 - t, 3) * p1;
        Vector3 part2 = 3 * Mathf.Pow(1 - t, 2) * t * p2;
        Vector3 part3 = 3 * (1 - t) * Mathf.Pow(t, 2) * p3;
        Vector3 part4 = Mathf.Pow(t, 3) * p4;

        return part1 + part2 + part3 + part4;
    }
}
