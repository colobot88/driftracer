﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MainButtonController : MonoBehaviour {

    public Sprite colorActive;
    public Sprite colorPassive;
    public Sprite rimActive;
    public Sprite rimPassive;

    public Image colorImage;
    public Image rimImage;

    public Color active;
    public Color passive;

    public GameObject colorPanel;
    public GameObject rimPanel;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void ColorClicked()
    {
        colorImage.color = active;
        colorImage.sprite = colorActive;
        colorPanel.SetActive(true);
        rimImage.sprite = rimPassive;
        rimImage.color = passive;
        rimPanel.SetActive(false);
    }

    public void RimClicked()
    {
        rimImage.sprite = rimActive;
        rimImage.color = active;
        rimPanel.SetActive(true);
        colorImage.sprite = colorPassive;
        colorPanel.SetActive(false);
        colorImage.color = passive;
    }
}
