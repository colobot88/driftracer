﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class MainScene : MonoBehaviour {

    //UI Objects
    public GameObject soundOn; //gameobject enabled when sound on
    public GameObject soundOff; //gameobject enabled when sound off
    public Image leaderBoard;
    public Text loggedin;

    //UI Text - will be updated when language changed
    public Text playText;
    public Text languageText;

    public InfoPanel infoPanel;

    void Start()
    {
        if (!GameManager.Instance.isMusicPlaying())
            ChangeSound(false);

        LeaderboardController.Instance.OnLoginChanged += Instance_OnLoginChanged;

        //Register onlanguagechanged event to update texts when lang changed
        Language.OnLanguageChanged += Language_OnLanguageChanged;
        Language_OnLanguageChanged(Language.CurrentLanguage);
    }

    private void Instance_OnLoginChanged(bool loggedin)
    {
        if (loggedin)
        {
            this.loggedin.text = "true";
        }
        else
        {
            this.loggedin.text = "false";
        }
    }

    private void Language_OnLanguageChanged(Languages current)
    {
        playText.text = Language.GetLanguageDictionary(current)[LanguageKeys.PLAY];
        languageText.text = Language.GetLanguageDictionary(current)[LanguageKeys.LANGUAGE];

        GameManager.Instance.DataManager.PlayerData.CurrentLanguage = (int)current;

        GameManager.Instance.DataManager.SavePlayerData();
    }

    public void Play(int sceneNo)
    {
        Language.OnLanguageChanged -= Language_OnLanguageChanged;
        GameManager.Instance.LoadScene(1);
    }

    public void ChangeSound(bool on)
    {
        GameManager.Instance.PlayMusic(on);
        soundOn.SetActive(!on);
        soundOff.SetActive(on);
    }

    public void ChangeLanguage()
    {
        Language.ChangeLanguage();
    }

    public void ShowHighScoresLeaderBoard()
    {
        int highScore = GameManager.Instance.DataManager.PlayerData.HighestScore;
        if (LeaderboardController.Instance.isLoggedIn)
        {
            LeaderboardController.Instance.AddHighScoreToLeaderBoard(highScore);
            SendCarsHighScores();
            LeaderboardController.Instance.ShowHighScoresLeaderBoard();
        }
        else
            LeaderboardController.Instance.LogIn();
    }

    public void SendCarsHighScores()
    {
        Debug.LogWarning("SendCarsHighScores");
        foreach(DrScore drScore in GameManager.Instance.DataManager.PlayerData.Scores)
        {
            Debug.Log(drScore.lbId + " " + drScore.score);
            if(!drScore.sent)
                LeaderboardController.Instance.AddHighScoreToLeaderBoardWithId(drScore.lbId, drScore.score);
        }
    }
}
