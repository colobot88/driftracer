﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RimUIController : MonoBehaviour {

    public List<RimButton> rims;

    internal void OnSelectedCarChanged(CarDataMono carData)
    {
        foreach (RimButton rb in rims)
        {
            if (carData.rimIds.Contains(rb.id))
            {
                rb.lockImage.SetActive(false);
            }
            else
            {
                rb.lockImage.SetActive(true);
            }
        }
    }

    internal RimButton GetRimDataById(int currentRimDataId)
    {
        foreach (RimButton rb in rims)
        {
            if (rb.id == currentRimDataId)
                return rb;
        }
        return null;
    }
}
