﻿Shader "MTE/VertexColored/ColorOnly"
{
	SubShader
	{
		Tags
		{
			"Queue" = "Geometry-99"
			"RenderType" = "Opaque"
		}
		LOD 200

		CGPROGRAM
		#pragma surface surf Lambert

		sampler2D _MainTex;

		struct Input
		{
			float3 color: Color;
		};

		void surf (Input IN, inout SurfaceOutput o)
		{
			o.Albedo = IN.color.rgb;
			o.Alpha = 1.0;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
