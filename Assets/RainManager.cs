﻿using UnityEngine;
using System.Collections;

public class RainManager : MonoBehaviour {

    public Light sun;

    public AudioClip thunderSound1;
    public AudioClip thunderSound2;
    public AudioClip thunderSound3;
    public AudioClip thunderSound4;
    public AudioClip thunderSound5;

    public AudioSource soundComponent;

    public GameObject lightningBolt1;
    public Transform lightningSpawn;
    public float lightningIntensity;
    public bool fadeLightning = true;
    public int lightningMinChance = 5;
    public int lightningMaxChance = 10;

    public float minIntensity;
    public float maxIntensity;
    public float lightningFlashLength = 2f;

    public float onTimer = 0f;
    public float timer = 0f;
    public float lightningTime = 10f;
    public bool lightingGenerated = false;
    public bool lightningEnabled = true;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        Lightning();
    }

    void Lightning()
    {
        timer += Time.deltaTime;

        if (timer >= lightningTime && lightingGenerated == false)
        {
            lightningTime = Random.Range(5, 15);

            //Added
            if (lightningEnabled)
            {
                sun.enabled = true;
            }

            lightingGenerated = true;


            int lightningNumber = Random.Range(1, 5);

            if (lightningNumber == 1)
            {
                soundComponent.PlayOneShot(thunderSound1);
            }

            if (lightningNumber == 2)
            {
                soundComponent.PlayOneShot(thunderSound2);
            }

            if (lightningNumber == 3)
            {
                soundComponent.PlayOneShot(thunderSound3);
            }

            if (lightningNumber == 4)
            {
                soundComponent.PlayOneShot(thunderSound4);
            }

            if (lightningNumber == 5)
            {
                soundComponent.PlayOneShot(thunderSound5);
            }

            //lightningSpawn.position = transform.position + transform.forward * 50;
            //Instantiate(lightningBolt1, lightningSpawn.position, lightningSpawn.rotation);
        }

        if (lightingGenerated == true)
        {
            if (fadeLightning == false)
            {
                sun.intensity += .22f;
            }

            //


            if (sun.intensity >= lightningIntensity && fadeLightning == false)
            {
                sun.intensity = lightningIntensity;
                fadeLightning = true;
            }
        }

        if (fadeLightning == true)
        {

            onTimer += Time.deltaTime;



            if (onTimer >= lightningFlashLength)
            {
                sun.intensity -= .14f;
            }


            if (sun.intensity <= 0)
            {
                sun.intensity = 0;
                fadeLightning = false;
                lightingGenerated = false;
                timer = 0;
                onTimer = 0;
                sun.enabled = false;
                sun.transform.rotation = Quaternion.Euler(50, Random.Range(0, 360), 0);

                lightningTime = Random.Range(lightningMinChance, lightningMaxChance);
                lightningIntensity = Random.Range(minIntensity, maxIntensity);
            }
        }

        //Added
        if (!lightningEnabled && lightingGenerated)
        {
            sun.intensity = 0;
            fadeLightning = false;
            lightingGenerated = false;
            timer = 0;
            onTimer = 0;
            sun.enabled = false;
            sun.transform.rotation = Quaternion.Euler(50, Random.Range(0, 360), 0);
            lightningTime = Random.Range(lightningMinChance, lightningMaxChance);
            lightningIntensity = Random.Range(minIntensity, maxIntensity);
        }
    }
}
