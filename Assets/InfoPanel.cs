﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class InfoPanel : MonoBehaviour {

    public Text infoText;
    public Transform loadingCircle;
    public GameObject yesButton;
    public Text yesText;
    public GameObject laterButton;
    public Text laterText;
    public bool loading = false;

    public void SetText(string info)
    {
        this.infoText.text = info;
    }

    public void OpenClosePanel(bool open, bool useLoading = false, bool yesLaterEnabled = false)
    {
        Debug.Log("info panel enabled " + useLoading);
        this.loading = useLoading && open;
        if (yesLaterEnabled)
        {
            yesText.text = Language.GetLanguageDictionary(Language.CurrentLanguage)[LanguageKeys.YES];
            laterText.text = Language.GetLanguageDictionary(Language.CurrentLanguage)[LanguageKeys.LATER];
            yesButton.SetActive(yesLaterEnabled);
            laterButton.SetActive(yesLaterEnabled);
        } 

        gameObject.SetActive(open);
    }

    public void ClosePanel()
    {
        this.loading = false;
        gameObject.SetActive(false);
    }

    void Update()
    {
        if(this.loading)
        {
            loadingCircle.Rotate(0, 0, 220 * Time.deltaTime);
        }
    }

    public void NoRateClicked()
    {
        GameManager.Instance.askRate = false;
        ClosePanel();
    }

    public void RateClicked()
    {
        #if UNITY_ANDROID
                Application.OpenURL("market://details?id=com.playoffgames.driftracer");
#elif UNITY_IPHONE
                     Application.OpenURL("itms-apps://itunes.apple.com/app/idYOUR_APP_ID");
#endif
        GameManager.Instance.askRate = false;
        //GameManager.Instance.RateClicked();

        ClosePanel();
    }

}
