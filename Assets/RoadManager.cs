﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class RoadManager : MonoBehaviour {

    #region Variables
    public List<GameObject> roadObjects; //list of roadObjects (terrain objects)
    private int currentRoadIndex = 0;
    private Road oldRoad;
    private Road currentRoad;
    private Road nextRoad;
    #endregion

    #region Properties
    public Road CurrentRoad
    {
        get
        {
            return currentRoad;
        }

        set
        {
            currentRoad = value;
        }
    }

    public Road NextRoad
    {
        get
        {
            return nextRoad;
        }

        set
        {
            nextRoad = value;
        }
    }

    public Road OldRoad
    {
        get
        {
            return oldRoad;
        }

        set
        {
            oldRoad = value;
        }
    }
    #endregion

    #region Events
    public delegate void OnLastRoadReachedEvent();
    public event OnLastRoadReachedEvent OnLastRoadReached;

    public delegate void OnRoadChangedEvent(int roadNo);
    public event OnRoadChangedEvent OnRoadChanged;
    #endregion

    //ınitialize road
    void Awake () {
    }

    public Road GetNextRoad(bool shouldEnable = true)
    {
        if (OldRoad != null)
            OldRoad.gameObject.SetActive(false);
        OldRoad = CurrentRoad;
        GameObject terrain = roadObjects[currentRoadIndex];
        terrain.SetActive(shouldEnable);

        Road road = terrain.GetComponent<Road>();
        CurrentRoad = road;

        if (OnRoadChanged != null)
            OnRoadChanged(currentRoadIndex);

        currentRoadIndex++;
        if (currentRoadIndex >= roadObjects.Count)
        {
            if (OnLastRoadReached != null)
                OnLastRoadReached();
        }
        else
            roadObjects[currentRoadIndex].SetActive(true);

        return road;
    }

    internal void Reset()
    {
        currentRoadIndex = 0;
        foreach (GameObject ro in roadObjects)
            ro.SetActive(false);
        OldRoad = null;
        CurrentRoad = null;
        CurrentRoad = GetNextRoad();
    }
}
