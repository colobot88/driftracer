﻿using UnityEngine;
using System.Collections;
using GooglePlayGames;
using GooglePlayGames.BasicApi.SavedGame;
using UnityEngine.SocialPlatforms;
using System;

public class LeaderboardController : MonoBehaviour
{
    #region LeaderboardController Instance
    private static LeaderboardController _instance = null; //singleton instance
    public static LeaderboardController Instance
    {
        get
        {
            return _instance;
        }
    }
    #endregion

    #region PUBLIC_VAR
    public string highScoresleaderboard;
    public string winnerTimesleaderboard;

    public bool isLoggedIn = false;

    // Event Handler
    public delegate void OnLoginChangedEvent(bool loggedin);
    public event OnLoginChangedEvent OnLoginChanged;
    #endregion

    #region Initialization
    void Awake()
    {
        if (!_instance)
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
            Destroy(gameObject);
    }
    #endregion

    #region DEFAULT_UNITY_CALLBACKS
    void Start()
    {
        // recommended for debugging:
        PlayGamesPlatform.DebugLogEnabled = true;

        //GooglePlayGames.BasicApi.PlayGamesClientConfiguration config = new GooglePlayGames.BasicApi.PlayGamesClientConfiguration.Builder().EnableSavedGames().Build();
        //PlayGamesPlatform.InitializeInstance(config);

        // Activate the Google Play Games platform
        PlayGamesPlatform.Activate();

        LogIn();
    }
    #endregion

    #region BUTTON_CALLBACKS
    /// <summary>
    /// Login In Into Your Google+ Account
    /// </summary>
    public void LogIn()
    {
        Social.localUser.Authenticate((bool success) =>
        {
            if (success)
            {
                isLoggedIn = true;
                Debug.Log("Login Sucess");
            }
            else
            {
                isLoggedIn = false;
                Debug.Log("Login failed");
            }
            if (OnLoginChanged != null)
                OnLoginChanged(isLoggedIn);
        });
    }

    /// <summary>
    /// Shows All Available Leaderborad
    /// </summary>
    public void ShowHighScoresLeaderBoard()
    {
        //        Social.ShowLeaderboardUI (); // Show all leaderboard
        if (((PlayGamesPlatform)Social.Active).IsAuthenticated())
        {
            ((PlayGamesPlatform)Social.Active).ShowLeaderboardUI(highScoresleaderboard); // Show current (Active) leaderboard
        }
        else
        {
            if (OnLoginChanged != null)
                OnLoginChanged(isLoggedIn);
            LogIn();
        }
    }

    public void ShowLeaderboardWithId(string id)
    {
        if (((PlayGamesPlatform)Social.Active).IsAuthenticated())
        {
            ((PlayGamesPlatform)Social.Active).ShowLeaderboardUI(id); // Show current (Active) leaderboard
        }
        else
        {
            if (OnLoginChanged != null)
                OnLoginChanged(isLoggedIn);
            LogIn();
        }
    }

    /// <summary>
    /// Adds Score To leader board
    /// </summary>
    public void AddHighScoreToLeaderBoard(int highScore)
    {

        if (Social.localUser.authenticated)
        {
            Social.ReportScore(highScore, highScoresleaderboard, (bool success) =>
            {
                if (success)
                {
                    Debug.Log("Update Score Success");

                }
                else
                {
                    Debug.Log("Update Score Fail");
                }
            });
        }
    }

    public void AddHighScoreToLeaderBoardWithId(string LbId, int highScore)
    {
        if (Social.localUser.authenticated)
        {
            Social.ReportScore(highScore, LbId, (bool success) =>
            {
                if (success)
                {
                    Debug.Log("Update car Score Success " + LbId);
                    GameManager.Instance.DataManager.PlayerData.SetScoreWithLbId(LbId, highScore, true);
                    GameManager.Instance.DataManager.SavePlayerData();
                }
                else
                {
                    Debug.Log("Update Score Fail");
                }
            });
        }
    }

    /// <summary>
    /// On Logout of your Google+ Account
    /// </summary>
    public void OnLogOut()
    {
        ((PlayGamesPlatform)Social.Active).SignOut();
    }
    #endregion

    private bool isSaving = false;
    private void SaveCloud()
    {
        if (Social.localUser.authenticated)
        {
            isSaving = true;
            ((PlayGamesPlatform)Social.Active).SavedGame.OpenWithAutomaticConflictResolution
                ("autosave", 
                GooglePlayGames.BasicApi.DataSource.ReadCacheOrNetwork,
                ConflictResolutionStrategy.UseLongestPlaytime,
                SaveGameOpened);
        } else
        {
            Debug.Log("could not saved");
        }
    }

    private void SaveGameOpened(SavedGameRequestStatus status, ISavedGameMetadata game)
    {
        if(status == SavedGameRequestStatus.Success)
        {
            if (isSaving)
            {
                byte[] data = System.Text.ASCIIEncoding.ASCII.GetBytes(GameManager.Instance.DataManager.PlayerData.ToString());
                TimeSpan playedTime = TimeSpan.FromSeconds(6161);
                SavedGameMetadataUpdate.Builder builder = new SavedGameMetadataUpdate.Builder().WithUpdatedPlayedTime(playedTime)
                    .WithUpdatedDescription("Save Game at: " + DateTime.Now );
                SavedGameMetadataUpdate update = builder.Build();
                ((PlayGamesPlatform)Social.Active).SavedGame.CommitUpdate(game, update, data, SavedGameWritten);
            }
            else
            {
                ((PlayGamesPlatform)Social.Active).SavedGame.ReadBinaryData(game, SavedGameLoaded);
            }
        } else
        {
            Debug.Log("error ");
        }
    }

    private void SavedGameWritten(SavedGameRequestStatus status, ISavedGameMetadata game)
    {
        Debug.Log(status);
    }

    private void SavedGameLoaded(SavedGameRequestStatus status, byte[] data)
    {
        if(status == SavedGameRequestStatus.Success)
        {
            LoadFromString(System.Text.ASCIIEncoding.ASCII.GetString(data));
        }
    }

    private void LoadFromString(string v)
    {
        throw new NotImplementedException();
    }

    private void LoadCloud()
    {
        isSaving = false;
        ((PlayGamesPlatform)Social.Active).SavedGame.OpenWithAutomaticConflictResolution
                ("autosave",
                GooglePlayGames.BasicApi.DataSource.ReadCacheOrNetwork,
                ConflictResolutionStrategy.UseLongestPlaytime,
                SaveGameOpened);
    }
}
