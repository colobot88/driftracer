﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Utility;
using System;
using UnityStandardAssets.Vehicles.Car;
using System.Collections.Generic;

public class CarManager : MonoBehaviour {

    private RoadManager roadManager;
    private WeatherManager weatherManager;

    public Rigidbody carRB;
    public GameObject car;
    private CarController carController;
    public CarAIControl carControl;
    private WheelCollider[] wheelColliders;
    public List<AudioSource> audioSources;
    public WaypointProgressTracker wpTracker;
    private float lastInput = 0f;
    private float addTime = 0f;
    private float totalDriftTime = 0f;
    private float driftTime = 0f;
    public PowerUps powerUps;
    private Vector3 lastPosition;
    private float totalDistance = 0f;
    public float offRoadTime = 0f;

    private float testDistance = 0f;
    private int testIndex = 0;

    private float currentSlipAmount = 0f;
    private float currentDistance = 0f;

    private float liveScore;

    // Events
    public delegate void OnTimePointEvent(Vector3 carPosition, float time);
    public event OnTimePointEvent OnTimePoint;

    public delegate void OnScoreChangedEvent(float score);
    public event OnScoreChangedEvent OnScoreChanged;

    //camera
    public SmoothFollow camFollow;

    public PowerUps PowerUps
    {
        get
        {
            return powerUps;
        }

        set
        {
            powerUps = value;
        }
    }

    public float TotalDistance
    {
        get
        {
            return totalDistance;
        }

        set
        {
            totalDistance = value;
        }
    }

    public CarController CarController
    {
        get
        {
            return carController;
        }

        set
        {
            carController = value;
        }
    }

    public float TotalDriftTime
    {
        get
        {
            return totalDriftTime;
        }

        set
        {
            totalDriftTime = value;
        }
    }

    public float LiveScore
    {
        get
        {
            return liveScore;
        }

        set
        {
            liveScore = value;
            if (OnScoreChanged != null)
            {
                OnScoreChanged(liveScore);
            }
        }
    }

    public void InitializeManager(RoadManager roadManager, WeatherManager weatherManager)
    {
        this.roadManager = roadManager;
        this.weatherManager = weatherManager;
        GamePlayManager.Instance.GameState.OnGameStateChanged += GameState_OnGameStateChanged;
        InitializeCar();
    }

    private void GameState_OnGameStateChanged(GAMESTATES gs)
    {
        if(gs == GAMESTATES.FINISH)
        {
            camFollow.target = null;
        }
    }

    void Update()
    {
        currentDistance = 0f;
        float currentSlipTime = 0f;
        if (GamePlayManager.Instance.GameState.CurrentGameState == GAMESTATES.PLAYING)
        {
            currentDistance = Mathf.Abs((car.transform.position - lastPosition).magnitude);
            TotalDistance += currentDistance;
            testDistance += Mathf.Abs((car.transform.position - lastPosition).magnitude);
            if (testDistance > 100)
            {
                testDistance = 0f;
                testIndex++;
            }
            if (wpTracker.progressNum >= wpTracker.circuit.waypointList.items.Length - 2)
            {
                GamePlayManager.Instance.RoadManager.GetNextRoad(); // nextRoad;
                wpTracker.circuit = GamePlayManager.Instance.RoadManager.CurrentRoad.wpCircuit;
                wpTracker.Reset();
            }
            if (car.transform.InverseTransformPoint(wpTracker.target.position).z < 0)
            {
                GamePlayManager.Instance.GameState.CurrentGameState = GAMESTATES.WRONG_WAY;
            }
            currentSlipAmount = 0f;
            currentSlipTime = CheckWheelsOnGround();
        }
        lastPosition = car.transform.position;

        CalculateScore(currentDistance, currentSlipTime);
    }

    private void CalculateScore(float currentDistance, float currentSlipTime)
    {
        float speedSqrFactor = ((CarController.GetCurrentSpeedFloat() * CarController.GetCurrentSpeedFloat() / 10750));
        if (currentSlipAmount > .05f)
            LiveScore += Time.deltaTime * 60 * currentDistance * currentSlipAmount * currentSlipAmount * (CarController.GetCurrentSpeedFloat() * CarController.GetCurrentSpeedFloat() / 150000) * TotalDistance;
        else
            LiveScore += Time.deltaTime * 60 * currentDistance * .05f * .05f * (CarController.GetCurrentSpeedFloat() / 15000f) * TotalDistance;
    }

    private void InitializeCar()
    {
        //initialize car
        if (GameManager.Instance == null)
        {
            car = Instantiate(Resources.Load("cars/car0/" + "car0", typeof(GameObject))) as GameObject;

            MonoBehaviour[] comps = car.GetComponents<MonoBehaviour>();
            foreach (MonoBehaviour c in comps)
            {
                c.enabled = true;
                if (c is PowerUps)
                {
                    PowerUps = (c as PowerUps);
                    if (PowerUps != null)
                    {
                        PowerUps.Initialize();
                    }
                }
            }
        }
        else
        {
            car = GameManager.Instance.GetCurrentCarObj();
            car.transform.parent = null;
            MonoBehaviour[] comps = car.GetComponents<MonoBehaviour>();
            foreach (MonoBehaviour c in comps)
            {
                c.enabled = true;
                if (c is PowerUps)
                {
                    PowerUps = (c as PowerUps);
                    if (PowerUps != null)
                    {
                        PowerUps.Initialize();
                    }
                }
            }
        }
        camFollow.target = car.transform;

        carRB = car.GetComponent<Rigidbody>();
        car.transform.position = roadManager.CurrentRoad.initialPositionRefTransform.position;
        car.transform.rotation = roadManager.CurrentRoad.initialPositionRefTransform.rotation;
        wpTracker = car.GetComponent<WaypointProgressTracker>();
        wpTracker.circuit = roadManager.CurrentRoad.wpCircuit;
        wpTracker.Reset();
        CarController = car.GetComponent<CarController>();
        carController.OnReachEndOfRoad += CarController_OnReachEndOfRoad;
        carControl = car.GetComponent<CarAIControl>();
        wheelColliders = car.GetComponent<CarController>().m_WheelColliders;
        audioSources = car.GetComponent<CarAudio>().addedAudioSources;

        AudioSource[] asources = car.GetComponentsInChildren<AudioSource>();
        foreach (AudioSource asource in asources)
            audioSources.Add(asource);
        car.SetActive(true);
        weatherManager.transform.parent = car.transform;
        weatherManager.transform.localPosition = Vector3.zero;
    }

    private void CarController_OnReachEndOfRoad()
    {
        GamePlayManager.Instance.GameState.CurrentGameState = GAMESTATES.FINISH;
    }

    private float CheckWheelsOnGround()
    {
        int wheelOnFlyCount = 0; //# of wheels not on ground
        float maxSlipValue = 0f; //max of wheels' slip value
        for (int i = 0; i < 4; i++)
        {
            WheelHit wheelhit;
            bool onground = wheelColliders[i].GetGroundHit(out wheelhit);

            RaycastHit hit;
            float dist = 2;
            Vector3 dir = new Vector3(0, -1, 0);
            if (Physics.Raycast(wheelColliders[i].transform.position, dir, out hit, dist))
            {
                if (hit.collider.tag != "Road")
                    onground = false;
                else if (Math.Abs(wheelhit.sidewaysSlip) >= CarController.m_SlipLimit)
                {
                    if (Math.Abs(wheelhit.sidewaysSlip) > maxSlipValue)
                    {
                        maxSlipValue = Math.Abs(wheelhit.sidewaysSlip);
                    }
                }
            }

            if (!onground)
                wheelOnFlyCount++;
        }
        currentSlipAmount = maxSlipValue;
        if (maxSlipValue > 0f)
        {
            TotalDriftTime += Time.deltaTime;
            driftTime += Time.deltaTime;
            addTime += maxSlipValue * Time.deltaTime * Mathf.Sqrt(CarController.GetCurrentSpeedFloat()) / 4;
            PowerUps.Nitrous.Amount += addTime / 5;
            if (addTime > 1f)
            {
                float speedSqrFactor = ((CarController.GetCurrentSpeedFloat() * CarController.GetCurrentSpeedFloat() / 10750));

                addTime *= speedSqrFactor;
                driftTime = 1 + (currentDistance)/2;
                addTime *= driftTime;
                GamePlayManager.Instance.TimeManager.IncreaseTimeLeft(addTime);
                if (OnTimePoint != null)
                    OnTimePoint(car.transform.position, addTime);
                addTime = 0f;
            }
        }
        else
        {
            driftTime = 0f;
        }

        if (wheelOnFlyCount >= 2)
        {
            offRoadTime += Time.deltaTime;
            if (offRoadTime > 4f || 
                (wheelOnFlyCount >= 3 && offRoadTime > 2f) || 
                (wheelOnFlyCount >= 4 && offRoadTime > .5f))
            {
                GamePlayManager.Instance.GameState.CurrentGameState = GAMESTATES.OFF_ROAD;
            }
        }
        else
            offRoadTime = 0f;

        return currentSlipAmount;
    }

    public void Reset()
    {
        camFollow.target = car.transform;
        CarController.Reset();
        GameObject skidParent = GameObject.Find("Skid Trails - Detached");
        if (skidParent != null)
        {
            foreach (Transform go in skidParent.transform)
                Destroy(go.gameObject);
        }
        offRoadTime = 0f;
        powerUps.Nitrous.Reset();
        SetCarSoundOnOff(true);
        car.SetActive(false);
        wpTracker.circuit = roadManager.CurrentRoad.wpCircuit;
        wpTracker.Reset();
        TotalDistance = 0f;
        driftTime = 0f;
        LiveScore = 0f;
        car.transform.position = roadManager.CurrentRoad.initialPositionRefTransform.position;
        car.transform.rotation = roadManager.CurrentRoad.initialPositionRefTransform.rotation;
        car.SetActive(true);
    }

    internal void OnPlaybackClicked()
    {
        offRoadTime = 0f;
        SetCarSoundOnOff(true);
        car.SetActive(false);
        car.SetActive(true);
    }

    public void SetCarSoundOnOff(bool enabled)
    {
        foreach (AudioSource asource in audioSources)
            asource.enabled = enabled;
    }

    public CarDataMono GetCarDataMono()
    {
        return this.car.GetComponent<CarDataMono>();
    }
}
