﻿using UnityEngine;
using System.Collections;
using System;

public class WeatherManager : MonoBehaviour {

    public Light sun;
    public GameObject rainObj;
    public GameObject snowObj;
    public WEATHER_TYPE weather_type = WEATHER_TYPE.SUNNY;

    void Start()
    {
        GamePlayManager.Instance.roadManager.OnRoadChanged += RoadManager_OnRoadChanged;
    }

    private void RoadManager_OnRoadChanged(int roadNo)
    {
        Debug.Log(roadNo);
        if (roadNo == 0 || roadNo == 1)
        {
            rainObj.SetActive(false);
            snowObj.SetActive(false);
        } else if(roadNo == 2 || roadNo == 3 || roadNo == 4)
        {
            rainObj.SetActive(true);
            snowObj.SetActive(false);
        } else
        {
            rainObj.SetActive(false);
            snowObj.SetActive(true);
        }
    }

    internal void Reset()
    {
        rainObj.SetActive(false);
        snowObj.SetActive(false);
    }
}

public enum WEATHER_TYPE
{
    SUNNY,
    RAIN,
    SNOW
}
