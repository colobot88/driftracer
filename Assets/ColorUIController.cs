﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityStandardAssets.Vehicles.Car;

public class ColorUIController : MonoBehaviour {

    public List<ColorButton> colors;

    internal void OnSelectedCarChanged(CarDataMono carData)
    {
        foreach(ColorButton cb in colors)
        {
            if (carData.colorIds.Contains(cb.id))
            {
                cb.lockImage.SetActive(false);
            }else
            {
                cb.lockImage.SetActive(true);
            }
        }
    }

    internal ColorButton GetColorDataById(int currentColorDataId)
    {
        foreach(ColorButton cb in colors)
        {
            if (cb.id == currentColorDataId)
                return cb;
        }
        return null;
    }
}
