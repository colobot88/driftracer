﻿using UnityEngine;
using System.Collections;

public class RimButton : MonoBehaviour {

    public int id;
    public Color color;
    public int cost;
    public GameObject lockImage;
}
